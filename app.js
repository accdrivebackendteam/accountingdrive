var express = require('express');
var sockjs  = require('sockjs');
var http    = require('http');
var db=require('./model/db');
var user = require('./model/user');
var company = require('./model/company');

var ruser=require('./routes/user');
var license=require('./routes/crlicense');
var sufunc=require('./routes/sufunction');
var passwd = require('./routes/passwordencryption');
var rights = require('./routes/rights');
var company = require('./routes/company');



// 1. Echo sockjs server
var sockjs_opts = {sockjs_url,function(data){ "http://cdn.sockjs.org/sockjs-0.3.min.js"};
var connections=Array();
var sockjs_echo = sockjs.createServer(sockjs_opts);
sockjs_echo.on('connection', function(conn) {
	connections.push(conn);
	for(i=0;i<connections.length;i++)
		connections[i].write("NEW MSG/CONNECTION");
	
	conn.on('close', function(){
	console.log('in close');
	var connid = conn.id;
	ruser.logoutuser(connid,function(reply){
		console.log("On Close"+JSON.stringify(reply));
	});
	/*sufunc.sulogout(connid,function(reply){
		console.log("On Close"+JSON.stringify(reply));
	});*/
	});    
	
    conn.on("su_createlicense",function(data){
		var connid = conn.id;
		license.addnewuserlicense(connid,data.email,data.registerfor,data.price,data.number,data.expireson,callback);
	});
	conn.on("getuserlicense",function(data){
		console.log('in su_getuserlicense');
		var connid = conn.id;
		license.getuserlicense(connid,callback);
	});
		
	conn.on("getcompanylicense",function(data){
		console.log('in su_getcompanylicense');
		var connid = conn.id;
		license.getcompanylicense(connid,callback);
	});
			
	 conn.on("su_deleteuserlicense",function(data){
		console.log('in su_deleteuserlicense');
		var connid = conn.id;
		sufunc.su_deleteuserlicense(connid,data.licenseid,callback);
		
	 conn.on("su_deletecompanylicense",function(data){
		console.log('in su_deletecompanylicense');
		var connid = conn.id;
		sufunc.su_deletecompanylicense(connid,data.licenseid,callback);
	});
			
	conn.on("su_edituserlicense",function(data){
		console.log('in su_edituserlicense');
		var connid = conn.id;
		sufunc.su_edituserlicense(connid,data.docid,data.useremail,data.userid,data.price,data.boughton,data.expireson,callback);
	});
	
	conn.on("su_editcompanylicense",function(data){
		console.log('in su_editcompanylicense');
		var connid = conn.id;
		sufunc.su_editcompanylicense(connid,data.docid,data.companyid,data.price,data.boughton,data.expireson,callback);
	});
			
	conn.on("generatepassword",function(data){
		passwd.encryptpassword(data,callback);
	});
			
	conn.on("checklogin",function(data){
		var IPaddress = conn.remoteAddress;
		var connid = conn.id;
		ruser.checklogin(data.email,data.password,IPaddress,connid,callback);
	});
			
	conn.on("suchecklogin",function(data){
		var IPaddress = conn.handshake.address.address;
		var connid = conn.id;
		sufunc.suchecklogin(data.email,data.password,IPaddress,connid,callback);
	});
			
	conn.on("hascreatecompanylicense",function(data){
		var connid = conn.id;
		ruser.hascreatecompanylicense(connid,callback);
	});
		
	conn.on("hasadduserlicense",function(data){
		var connid = conn.id;
		ruser.hasadduserlicense(connid,callback);
	});
		
	conn.on("createcompany",function(data){
		var connid = conn.id;
		ruser.createcompany(connid,callback);
	});
				
	conn.on("addusertocompany",function(data){
		var connid = conn.id;
		ruser.addusertocompany(connid,callback);
	});
				
	conn.on("logoutuser",function(data){
		var connid = conn.id;
		ruser.logoutuser(connid,callback);
	});
			
	conn.on("sulogout",function(data){
		var connid = conn.id;
		sufunc.sulogout(connid,callback);
	});
		
	conn.on("setuserrightsforcompany",function(data){
		var connid = conn.id;
		rights.setuserrightsforcompany(connid,data.companyid,data.userrights,callback);
	});
			
	conn.on("getuserrightsforcompany",function(data){
		rights.getuserrightsforcompany(data.userid,data.companyid,callback);
	});
		
	conn.on("addusertocompany",function(data){
		var connid = conn.id;
		company.addusertocompany(connid,data.companyid,data.emailid,callback);
	});
		
	conn.on("removeuserfromcompany",function(data){
		company.removeuserfromcompany(connid,data.email,data.companyid,callback);
	});
		
	conn.on("hasunusedcompanylicense",function(data){
		var connid = conn.id;
		company.hasunusedcompanylicense(connid,callback);
	});
	
	conn.on("getuserlicensedetails",function(data){
		var connid = conn.id;
		company.getuserlicensedetails(connid,callback);
	});		
		
    function callback(reply){
		console.log("Result from function"+JSON.stringify(reply));
        conn.emit(reply.action,reply.response);
    }
	
});

// 2. Express server
var app = express(); /* express.createServer will not work here */
var server = http.createServer(app);

sockjs_echo.installHandlers(server, {prefix,function(data){'/trial'});

console.log(' [*] Listening on 0.0.0.0:9999' );
server.listen(9999, '0.0.0.0');

app.get('/', function (req, res) {
    res.sendfile(__dirname + '/index.html');
});
/*
app.get('/agentlogin',ruser.agentlogin);

*/
