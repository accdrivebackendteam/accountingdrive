var mongoose=require('mongoose');

var sch_group=new mongoose.Schema(
{
	name:{ type: String, required: true },
	under:mongoose.Schema.ObjectId,
	isprimary:Boolean,
	isdefault:Boolean,
	nature:String,
	company:mongoose.Schema.ObjectId, 
	createdon:{ type: Date, default: new Date() },
	modifiedon:{ type: Date, default: new Date() },
	createdby:mongoose.Schema.ObjectId,
	modifiedby:mongoose.Schema.ObjectId
});

mongoose.model('mod_group',sch_group,'group');
