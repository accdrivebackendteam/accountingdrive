var mongoose=require('mongoose');

var sch_user=new mongoose.Schema(
{
	name:{ type: String },
	email:{ type: String, unique: true, required: true },
	password:{ type: String, required: true },
	createdon:{ type: Date, default: new Date() },
	modifiedon:{ type: Date, default: new Date() },
	usertype:String,
	loggedintime:[Date],
	loggedouttime:[Date],
	loggedinfromip:String,
	isloggedin:Boolean,
	currentresourceid:String,
	userrights:[{
				companyid:mongoose.Schema.ObjectId,
				isadmin:Boolean,
				companyuserrights:[{ 
				modulename:String,
				rights:String,
				createdon:{ type: Date, default: new Date() },
				modifiedon:Date
				}],
				createdon:{ type: Date, default: new Date() },
				modifiedon:{ type: Date, default: new Date() }
			}],
	companylicense:[{
			boughton:{ type: Date, default: new Date() },
		       price:Number,
		       expireson:Date,
		       companyid:mongoose.Schema.ObjectId,
		       companyname:String
			}],
	userlicense:[{
			email:String,
		  	boughton:{ type: Date, default: new Date() },
                       price:Number,
                       expireson:Date,
                       userid:mongoose.Schema.ObjectId
			}]
});

mongoose.model('mod_user',sch_user,'user');

var su_schema = new mongoose.Schema({
	email:String,
	password:String,
	isloggedin:Boolean,
	loggedinfromip:String,
	currentresourceid:String,
	loggedintime:[Date],
	loggedouttime:[Date]
	});

mongoose.model('superuser',su_schema,'superuser');
