function openCreateLicenseDialog()
{
        $("#modaldiv").load(home+"/Super%20User/License/su_create_license.html");
}
function openSelectPeriodDialog()
{
        $("#modaldiv").load(home+"/Company/select_period.html");
}

function openEditGroupDialog()
{
        $("#modaldiv").load(home+"/Masters/Groups/edit_group.html");
}

function openEditSimpleUnitDialog()
{
        $("#modaldiv").load(home+"/Masters/Units%20of%20Measurement/edit_simpleunit.html");
}

function openEditCompoundUnitDialog()
{
        $("#modaldiv").load(home+"/Masters/Units%20of%20Measurement/edit_compoundunit.html");
}

function openEditStockCategoryDialog()
{
        $("#modaldiv").load(home+"/Masters/Stock%20Categories/edit_stockcategory.html");
}

function gotoHomePage()
{
    /*
        $("#maindiv-content").load(home+"/entrypage.html");
        createCookie("accdrive_currentpage","entrypage.html","");
        */
}

function gotoGroups()
{
   	$("#contentdiv").load(home+"/Masters/Groups/group.html");
        createCookie("accdrive_currentpage", home+"/Masters/Groups/group.html", "");
}

function gotoLedgers()
{
   	$("#contentdiv").load(home+"/Masters/Ledger/ledger.html");
        createCookie("accdrive_currentpage", "Masters/Ledger/ledger.html", "");
}

function gotoVoucherTypes()
{
   	$("#contentdiv").load(home+"/Masters/Voucher%20Types/vouchertype.html");
        createCookie("accdrive_currentpage", "Masters/Voucher%20Types/vouchertype.html", "");
}

function gotoStockGroups()
{
   	$("#contentdiv").load(home+"/Masters/Stock%20Groups/stockgroup.html");
        createCookie("accdrive_currentpage", "Masters/Stock%20Groups/stockgroup.html", "");
}

function gotoStockCategories()
{
   	$("#contentdiv").load(home+"/Masters/Stock%20Categories/stockcategory.html");
        createCookie("accdrive_currentpage", "Masters/Stock%20Categories/stockcategory.html", "");
}

function gotoStockItems()
{
   	$("#contentdiv").load(home+"/Masters/Stock%20Items/stockitem.html");
        createCookie("accdrive_currentpage", "Masters/Stock%20Items/stockitem.html", "");
}

function gotoUnitsOfMeasure()
{
   	$("#contentdiv").load(home+"/Masters/Units%20of%20Measurement/units.html");
        createCookie("accdrive_currentpage", "Masters/Units%20of%20Measurement/units.html", "");
}

function gotoUserCompanyMapping()
{
        $("#contentdiv").load(home+"/AdminIndex/user_company_mapping.html");
        createCookie("accdrive_currentpage","AdminIndex/user_company_mapping.html","");
}
function gotoUserRights()
{
        $("#contentdiv").load(home+"/AdminIndex/user_rights.html");
        createCookie("accdrive_currentpage","AdminIndex/user_rights.html","");
}
function gotoUserLicense()
{
        $("#contentdiv").load(home+"/AdminIndex/user_license.html");
        createCookie("accdrive_currentpage","AdminIndex/user_license.html","");
}
function gotoCompanyLicense()
{
        $("#contentdiv").load(home+"/AdminIndex/company_license.html");
        createCookie("accdrive_currentpage","AdminIndex/company_license.html","");
}
function openCreateGroupDialog()
{
        $("#modaldiv").load(home+"/Masters/Groups/create_group.html");
}

function openCreateLedgerDialog()
{
        $("#modaldiv").load(home+"/Masters/Ledger/create_ledger.html");
}

function openCreateStockGroupDialog()
{
        $("#modaldiv").load(home+"/Masters/Stock%20Groups/create_stockgroup.html");
}

function openCreateStockCategoryDialog()
{
        $("#modaldiv").load(home+"/Masters/Stock%20Categories/create_stockcategory.html");
}

function openCreateStockItemDialog()
{
        $("#modaldiv").load(home+"/Masters/Stock%20Items/create_stockitem.html");
}

function openCreateUnitDialog()
{
        $("#modaldiv").load(home+"/Masters/Units%20of%20Measurement/create_unit.html");
}

function openCreateGodownDialog()
{
        $("#modaldiv").load(home+"/Masters/Godown/create_godown.html");
}

function openAccountingFeaturesDialog()
{
        $("#modaldiv").load(home+"/Masters/accounting_features.html");
}

function openMasterConfigurationDialog()
{
        $("#modaldiv").load(home+"/Masters/master_configuration.html");
}


function openCreateCompanyDialog()
{
   	$("#modaldiv").load(home+"/Company/create_company.html");
}

function openEditCompanyDialog()
{
   	$("#modaldiv").load(home+"/Company/edit_company.html");
}

function openSelectCompanyDialog()
{
   	$("#modaldiv").load(home+"/Company/select_company.html");
}

function slideDownStockItemCreation() 
{
        $("#dropdowndiv").load(home+"/Masters/Stock%20Items/create_stockitem.html");
        $("#dropdowndiv").slideDown(1000);
}

function slideDownVoucherTypeCreation() 
{
        $("#dropdowndiv").load(home+"/Masters/Voucher%20Types/create_vouchertype.html");
        $("#dropdowndiv").slideDown(1000);
}

function closeDiv(divId) 
{
        document.getElementById(divId).style.display = 'none';    
}

function gotoupdatecompany(selcomp)
{
	$("#contentdiv").load(home+"/Company/create_company.html?ID="+selcomp+"&ISEDIT=TRUE");
}
function gotodisplaycompany(selcomp)
{
	$("#contentdiv").load(home+"/Company/create_company.html?ID="+selcomp);
}
   
function gotocreatestockcategory()
{
	$("#contentdiv").load(home+"/stockcategory/create_stockcategory.html");
}
function gotovoucherentry()
{
	$("#contentdiv").load(home+"/voucher_entry/voucherentry.html");
}
function gotostockjournal()
{
	$("#contentdiv").load(home+"/voucher/stock_journal.html");
}
function openVoucherAdvanceConfiguration()
{
    $("#modaldiv").load(home+"/Masters/Voucher%20Types/create_vouchertype_secondary.html");
}