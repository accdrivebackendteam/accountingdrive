var execquery = require('./executequery');

exports.settoken = function settoken(oldconnid,newconnid,callback){
	var sql="update tbl_userlogin_log set connectionid='"+newconnid+"' where connectionid='"+oldconnid+"'";
	execquery.executequery(sql,function(token_data){
		if(token_data.error!=null)
		{
			callback(execquery.buildjson("settoken","Settoken query",token_data.error));
			return;
		}	
		var rowsAffected = token_data.affectedRows;
		callback(execquery.buildjson("settoken",newconnid,null));
	});
};

exports.getuserid = function getuserid(connid,callback){
	var sql="select userid,username from tbl_userlogin_log tul join tbl_users tu on tul.userid=tu.idtbl_users "+
	"where connectionid='"+connid+"' and isalive=1 order by startedat desc limit 1";
	execquery.executeselectquery(sql,function(user_data){
		if(user_data.error!=null)
		{
			callback(execquery.buildjson("getuserid","Settoken query",user_data.error));
			return;
		}	
		console.log('getuserid '+JSON.stringify(user_data));
		var user_id = user_data.data[0].userid;
		var user_email = user_data.data[0].username;
		callback(execquery.buildjson("getuserid",{"user_id":user_id,"user_email":user_email},null));
	});
};

exports.checkusertype = function checkusertype(connid,callback){
	var sql="select idtbl_users,issuperuser from tbl_users tu join tbl_userlogin_log tul on tu.idtbl_users=tul.userid "+
	" where tul.connectionid='"+connid+"' and tul.isalive=1 order by startedat desc limit 1";
	execquery.executeselectquery(sql,function(user_data){
		console.log('called BACK');
		console.log('called value'+JSON.stringify(user_data));
		if(user_data.error!=null)
		{
			console.log('');
			callback(execquery.buildjson("checkusertype","Settoken query",user_data.error));
			return;
		}	
		var userid = user_data.data[0].idtbl_users;
		var superuser = (user_data.data[0].issuperuser==0)?false:true;
		console.log('super '+superuser+'user '+userid);
		callback(execquery.buildjson("checkusertype",{"user_id":userid,"usertype":superuser},null));
	});
};

exports.checkifadminofcompany = function checkifadminofcompany(userid,companyid,callback){
	var sql="select count(*) cnt from tbl_company where administrator='"+userid+"' and companyid='"+companyid+"'";
	execquery.executeselectquery(sql,function(if_admin){
		if(if_admin.error!=null){
			callback(execquery.buildjson("checkifadminofcompany","Error in qry",if_admin.error));
			return;
		}
		is_admin = (if_admin.data[0].cnt==0)?false:true;
		callback(execquery.buildjson("checkifadminofcompany",is_admin,null));
	});
};
