var execquery = require('./executequery');

exports.getuserrightsforcompany = function getuserrightsforcompany(companyid,userid,callback){
	var sql="select permissionallowed from tbl_userpermissions where userid='"+userid+"' and companyid='"+companyid+"'";
	execquery.executeselectquery(sql,function(permission_data){
		if(permission_data.error!=null){
			callback(execquery.buildjson("getuserrightsforcompany","Error in select qry",permission_data.error));
			return;
		}
	});
	callback(execquery.buildjson("getuserrightsforcompany",permission_data.data,null));
};

exports.setuserrightsforcompany = function setuserrightsforcompany(userrights,user_id,companyid,upd_userid,callback){
	var rights = JSON.stringify({"groups":userrights.groups,"ledgers":userrights.ledgers,"vouchertypes":userrights.vouchertypes,		
	"inventoryinfo":userrights.inventoryinfo,"stockgroups":userrights.stockgroups,"stockcategories":
	userrights.stockcategories,"stockitems":userrights.stockitems,"unitsofmeasure":
	userrights.unitsofmeasure,"excise":userrights.excise,"excisefordealer":
	userrights.excisefordealer,"valueaddedtax":userrights.valueaddedtax,"taxdeductedatsource":
	userrights.taxdeductedatsource,"taxcollectedatsource":userrights.taxcollectedatsource,"servicetax":
	userrights.servicetax,"accountingvouchers":userrights.accountingvouchers,"inventoryvouchers":
	userrights.inventoryvouchers,"ordervouchers":userrights.ordervouchers,"importofdata":
	userrights.importofdata,"banking":userrights.banking,"trialbalance":userrights.trialbalance,
	"daybook":userrights.daybook,"accountsbook":userrights.accountsbook,"statementsofaccounts":
	userrights.statementsofaccounts,"inventorybooks":userrights.inventorybooks,"statementofinventory":
	userrights.statementofinventory,"cash/fundsflow":userrights.cashfundsflow,"receiptsandpayments":
	userrights.receiptsandpayments,"listofaccounts":userrights.listofaccounts,"exceptionreports":
	userrights,"balancesheet":userrights.exceptionreports,"profit&lossA/c":userrights.profit&lossac,
	"stocksummary":userrights.stocksummary,"ratioanalysis":userrights.ratioanalysis});
	
	var sql="insert into tbl_userpermissions(userid,permissionallowed,createdon,createdby,companyid) "+
	" values('"+upd_userid+"','"+rights+"',UTC_TIMESTAMP(),'"+user_id+"','"+companyid+"')";
	execquery.executequery(sql,function(perm_insert_data){
		if(perm_insert_data.error!=null){
			callback(execquery.buildjson("setuserrightsforcompany","Error in insert qry",perm_insert_data.error));
			return;
		}
	callback(execquery.buildjson("setuserrightsforcompany","Rights set to User",null));
	});
};

exports.checkuserrights = function checkuserrights(user_id,companyid,rightsfor,action,callback){
	var sql="select i.isadmin,IFNULL(j.permissionallowed,'') permissionallowed from (select (case when administrator='"+user_id+"' then 1 else 0 end) "+
	"isadmin,companyid from tbl_company where companyid='"+companyid+"')i left outer join (select permissionallowed,companyid "+
	"from tbl_userpermissions where userid='"+user_id+"' and companyid='"+companyid+"')j on i.companyid=j.companyid";
	execquery.executeselectquery(sql,function(has_rights){
		console.log('HAS RIGHTS '+JSON.stringify(has_rights));
		if(has_rights.error!=null){
			callback(execquery.buildjson("checkuserrights","Error in has rights",has_rights.error));
			return;
		}
		else if(has_rights.data[0].isadmin==1){
			callback(execquery.buildjson("checkuserrights",{"hasright":true},null));
			return;
		}
		else
			{
			var rights = JSON.parse(has_rights.data[0].permissionallowed);
			var to_be_checked = rights.rightsfor;
			var to_check = (to_be_checked.indexOf(action)>-1)?true:false;
			callback(execquery.buildjson("checkuserrights",{"hasright":to_check},null));
			}
	});
};
