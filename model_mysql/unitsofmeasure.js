var execquery = require('./executequery');

exports.getall_simple_unitsofmeasure = function getall_simple_unitsofmeasure(companyid,callback){
	var sql="select formalname,unitid,symbol,decimalplaces from tbl_unitsofmeasure where companyid='"+companyid+
	"' and deleted=0";
	execquery.executeselectquery(sql,function(get_all_units){
		console.log('IN QUERY CALLACK '+JSON.stringify(get_all_units));
		if(get_all_units.error!=null){
			callback(execquery.buildjson("getall_simple_unitsofmeasure","Error in select qry",get_all_units.error));
			return;
		}
		callback(execquery.buildjson("getall_simple_unitsofmeasure",get_all_units.data,null));
	});
};


exports.getdetails_simple_unitsofmeasure = function getdetails_simple_unitsofmeasure(unit_id,companyid,callback){
	var sql="select formalname,symbol,decimalplaces from tbl_unitsofmeasure where unitid='"+unit_id+
	"' and deleted=0 and companyid='"+companyid+"'";
	execquery.executeselectquery(sql,function(all_units){
		if(all_units.error!=null){
			callback(execquery.buildjson("getdetails_simple_unitsofmeasure","Error in select qry",all_units.error));
			return;
		}
		callback(execquery.buildjson("getdetails_simple_unitsofmeasure",all_units.data,null));
	});
};

exports.create_simple_unitsofmeasure = function create_simple_unitsofmeasure(companyid,user_id,unitsofmeasure,callback){
	var sql="insert into tbl_unitsofmeasure(companyid,symbol,formalname,decimalplaces,createdby,createdon) "+
	" values('"+companyid+"','"+unitsofmeasure.symbol+"','"+unitsofmeasure.formalname+"','"+
	unitsofmeasure.decimalplaces+"','"+user_id+"',UTC_TIMESTAMP())";
	execquery.executequery(sql,function(units_data){
		if(units_data.error!=null){
			callback(execquery.buildjson("create_simple_unitsofmeasure","Error in tbl_units insertion",units_data.error));
			return;
		}
		var unit_id = units_data.data.insertId;
		callback(execquery.buildjson("create_simple_unitsofmeasure",{"msg":"Unit Of Measure Created Successfully","unitid":unit_id},null));
	});
};

exports.update_simple_unitsofmeasure = function update_simple_unitsofmeasure(unit_id,user_id,unitsofmeasure,companyid,callback){
	var sql="update tbl_unitsofmeasure set symbol='"+unitsofmeasure.symbol+"',formalname='"+unitsofmeasure.formalname+
	"',decimalplaces='"+unitsofmeasure.decimalplaces+"',modifiedby='"+user_id+"',modifiedon=UTC_TIMESTAMP() "+
	" where unitid='"+unit_id+"' and companyid='"+companyid+"'";
	execquery.executequery(sql,function(upd_units_data){
		if(upd_units_data.error!=null){
			callback(execquery.buildjson("update_simple_unitsofmeasure","Error in tbl_units upd",upd_units_data.error));
			return;
		}
		callback(execquery.buildjson("update_simple_unitsofmeasure","Updated Succesfully",null));
	});
};

exports.delete_simple_unitsofmeasure = function delete_simple_unitsofmeasure(unit_id,user_id,companyid,callback){
	var sql="update tbl_unitsofmeasure set deletedby='"+user_id+"',deletedon=UTC_TIMESTAMP(),deleted=1 "+
	" where unitid='"+unit_id+"' and companyid='"+companyid+"'"
	execquery.executequery(sql,function(del_units_data){
		if(del_units_data.error!=null){
			callback(execquery.buildjson("delete_simple_unitsofmeasure","Error in tbl_units del",del_units_data.error));
			return;
		}
		callback(execquery.buildjson("delete_simple_unitsofmeasure","Deleted Successfully",null));
	});
};

exports.getall_compound_unitsofmeasure = function getall_compound_unitsofmeasure(companyid,callback){
	var sql="select t1.id,t1.conversion,t1.firstunitid,t1.secondunitid,t2.formalname firstunitname,t3.formalname "+
	" secondunitname,t1.companyid,t1.deleted from (select id,conversion,firstunitid,secondunitid,companyid,deleted "+
	" from tbl_unitsofmeasure_childdetails)t1 join (select unitid,formalname from tbl_unitsofmeasure)t2 on "+
	" t1.firstunitid=t2.unitid join (select unitid,formalname from tbl_unitsofmeasure)t3 on t1.secondunitid="+
	" t3.unitid where companyid='"+companyid+"' and deleted=0"
	execquery.executeselectquery(sql,function(get_all_units){
		console.log('IN QUERY CALLACK '+JSON.stringify(get_all_units));
		if(get_all_units.error!=null){
			callback(execquery.buildjson("getall_compound_unitsofmeasure","Error in select qry",get_all_units.error));
			return;
		}
		callback(execquery.buildjson("getall_compound_unitsofmeasure",get_all_units.data,null));
	});
};


exports.getdetails_compound_unitsofmeasure = function getdetails_compound_unitsofmeasure(unit_id,companyid,callback){
	var sql="select t1.id,t1.firstunitid,t1.secondunitid,t1.conversion,t2.formalname firstunitname,t3.formalname "+
	" secondunitname from (select id,conversion,firstunitid,secondunitid,companyid from tbl_unitsofmeasure_childdetails"+
	" )t1 join (select unitid,formalname from tbl_unitsofmeasure)t2 on t1.firstunitid=t2.unitid join (select unitid,"+
	" formalname from tbl_unitsofmeasure)t3 on t1.secondunitid=t3.unitid where id='"+unit_id+"' and companyid='"+
	companyid+"'";
	execquery.executeselectquery(sql,function(all_units){
		if(all_units.error!=null){
			callback(execquery.buildjson("getdetails_compound_unitsofmeasure","Error in select qry",all_units.error));
			return;
		}
		callback(execquery.buildjson("getdetails_compound_unitsofmeasure",all_units.data,null));
	});
};

exports.create_compound_unitsofmeasure = function create_compound_unitsofmeasure(companyid,user_id,unitsofmeasure,callback){
	var sql="insert into tbl_unitsofmeasure_childdetails(firstunitid,conversion,secondunitid,"+
	"createdon,createdby,companyid) values('"+unitsofmeasure.firstunitid+"','"+unitsofmeasure.conversion+"','"+unitsofmeasure.secondunitid
	+"',UTC_TIMESTAMP(),'"+user_id+"','"+companyid+"')";
	execquery.executequery(sql,function(units_data){
		if(units_data.error!=null){
			callback(execquery.buildjson("create_compound_unitsofmeasure","Error in tbl_units insertion",units_data.error));
			return;
		}
		var unit_id = units_data.data.insertId;
		callback(execquery.buildjson("create_compound_unitsofmeasure",{"msg":"Unit Of Measure Created Successfully","unitid":unit_id},null));
	});
};

exports.update_compound_unitsofmeasure = function update_compound_unitsofmeasure(unit_id,user_id,unitsofmeasure,companyid,callback){
	var sql="update tbl_unitsofmeasure_childdetails set conversion='"+unitsofmeasure.conversion+"',"+
	" firstunitid='"+unitsofmeasure.firstunitid+"',secondunitid='"+unitsofmeasure.secondunitid+"',modifiedby='"+
	user_id+"',modifiedon=UTC_TIMESTAMP() where id='"+unit_id+"' and companyid='"+companyid+"'";
	execquery.executequery(sql,function(upd_units_data){
		if(upd_units_data.error!=null){
			callback(execquery.buildjson("update_compound_unitsofmeasure","Error in tbl_units upd",upd_units_data.error));
			return;
		}
		callback(execquery.buildjson("update_compound_unitsofmeasure","Updated Succesfully",null));
	});
};

exports.delete_compound_unitsofmeasure = function delete_compound_unitsofmeasure(unit_id,user_id,companyid,callback){
	var sql="update tbl_unitsofmeasure_childdetails set deletedon=UTC_TIMESTAMP(),deletedby='"+user_id+
	"',deleted=1 where id='"+unit_id+"' and companyid='"+companyid+"'";
	execquery.executequery(sql,function(del_units_chd_data){
		if(del_units_chd_data.error!=null){
			callback(execquery.buildjson("delete_compound_unitsofmeasure","Error in tbl_units del",del_units_chd_data.error));
			return;
		}
		callback(execquery.buildjson("delete_compound_unitsofmeasure","Deleted Successfully",null));
	});
};
