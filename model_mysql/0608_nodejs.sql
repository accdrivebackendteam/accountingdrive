-- MySQL dump 10.14  Distrib 5.5.37-MariaDB, for Linux (i686)
--
-- Host: localhost    Database: nodejs
-- ------------------------------------------------------
-- Server version	5.5.37-MariaDB-wsrep

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_accountingvoucher`
--

DROP TABLE IF EXISTS `tbl_accountingvoucher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_accountingvoucher` (
  `accountingvoucherid` int(11) NOT NULL,
  `vouchertype` int(11) DEFAULT NULL,
  `reference` varchar(15) DEFAULT NULL,
  `date` date NOT NULL,
  `fromledgerid` int(11) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `narration` varchar(300) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `modifedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`accountingvoucherid`),
  KEY `fk_accountingvoucher_1` (`fromledgerid`),
  KEY `fk_accountingvoucher_2` (`vouchertype`),
  KEY `fk_tbl_accountingvoucher_1_idx` (`createdby`),
  KEY `fk_tbl_accountingvoucher_2_idx` (`modifedby`),
  CONSTRAINT `fk_accountingvoucher_10` FOREIGN KEY (`fromledgerid`) REFERENCES `tbl_ledger` (`ledgerid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_accountingvoucher_20` FOREIGN KEY (`vouchertype`) REFERENCES `tbl_vouchertype` (`voucherid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_accountingvoucher_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_accountingvoucher_2` FOREIGN KEY (`modifedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_accountingvoucher`
--

LOCK TABLES `tbl_accountingvoucher` WRITE;
/*!40000 ALTER TABLE `tbl_accountingvoucher` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_accountingvoucher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_accountingvoucherid`
--

DROP TABLE IF EXISTS `tbl_accountingvoucherid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_accountingvoucherid` (
  `voucherid` int(11) NOT NULL,
  `accountingvoucherid` int(11) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  PRIMARY KEY (`voucherid`),
  KEY `fk_accountingvoucherid_1` (`voucherid`),
  KEY `fk_tbl_accountingvoucherid_1_idx` (`createdby`),
  KEY `fk_tbl_accountingvoucherid_2_idx` (`modifiedby`),
  CONSTRAINT `fk_accountingvoucherid_10` FOREIGN KEY (`voucherid`) REFERENCES `tbl_accountingvoucher` (`accountingvoucherid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_accountingvoucherid_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_accountingvoucherid_2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_accountingvoucherid`
--

LOCK TABLES `tbl_accountingvoucherid` WRITE;
/*!40000 ALTER TABLE `tbl_accountingvoucherid` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_accountingvoucherid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_accountingvoucherto`
--

DROP TABLE IF EXISTS `tbl_accountingvoucherto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_accountingvoucherto` (
  `accountingvoucherid` int(11) DEFAULT NULL,
  `toledgerid` int(11) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `narration` varchar(150) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifedon` datetime DEFAULT NULL,
  KEY `fk_accountingvoucherto_1` (`accountingvoucherid`),
  KEY `fk_accountingvoucherto_2` (`toledgerid`),
  KEY `fk_tbl_accountingvoucherto_1_idx` (`createdby`),
  KEY `fk_tbl_accountingvoucherto_2_idx` (`modifiedby`),
  CONSTRAINT `fk_accountingvoucherto_10` FOREIGN KEY (`accountingvoucherid`) REFERENCES `tbl_accountingvoucher` (`accountingvoucherid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_accountingvoucherto_20` FOREIGN KEY (`toledgerid`) REFERENCES `tbl_ledger` (`ledgerid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_accountingvoucherto_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_accountingvoucherto_2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_accountingvoucherto`
--

LOCK TABLES `tbl_accountingvoucherto` WRITE;
/*!40000 ALTER TABLE `tbl_accountingvoucherto` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_accountingvoucherto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_classexcludegroups`
--

DROP TABLE IF EXISTS `tbl_classexcludegroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_classexcludegroups` (
  `classid` int(11) DEFAULT NULL,
  `groupid` int(11) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifedby` int(11) DEFAULT NULL,
  `modifedon` datetime DEFAULT NULL,
  KEY `fk_classexcludegroups_1` (`classid`),
  KEY `fk_classexcludegroups_2` (`groupid`),
  KEY `fk_tbl_classexcludegroups_1_idx` (`createdby`),
  KEY `fk_tbl_classexcludegroups_2_idx` (`modifedby`),
  CONSTRAINT `fk_classexcludegroups_10` FOREIGN KEY (`classid`) REFERENCES `tbl_vouchertypeclass` (`classid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_classexcludegroups_20` FOREIGN KEY (`groupid`) REFERENCES `tbl_group` (`groupid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_classexcludegroups_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_classexcludegroups_2` FOREIGN KEY (`modifedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_classexcludegroups`
--

LOCK TABLES `tbl_classexcludegroups` WRITE;
/*!40000 ALTER TABLE `tbl_classexcludegroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_classexcludegroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_classincludegroups`
--

DROP TABLE IF EXISTS `tbl_classincludegroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_classincludegroups` (
  `classid` int(11) NOT NULL,
  `groupid` int(11) NOT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  KEY `fk_classincludegroups_1` (`classid`),
  KEY `fk_classincludegroups_2` (`groupid`),
  KEY `fk_tbl_classincludegroups_1_idx` (`createdby`),
  KEY `fk_tbl_classincludegroups_2_idx` (`modifiedby`),
  CONSTRAINT `fk_classincludegroups_10` FOREIGN KEY (`classid`) REFERENCES `tbl_vouchertypeclass` (`classid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_classincludegroups_20` FOREIGN KEY (`groupid`) REFERENCES `tbl_group` (`groupid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_classincludegroups_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_classincludegroups_2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_classincludegroups`
--

LOCK TABLES `tbl_classincludegroups` WRITE;
/*!40000 ALTER TABLE `tbl_classincludegroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_classincludegroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_classledgers`
--

DROP TABLE IF EXISTS `tbl_classledgers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_classledgers` (
  `classid` int(11) NOT NULL,
  `ledgerid` int(11) NOT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `modifedby` int(11) DEFAULT NULL,
  KEY `fk_classledgers_1` (`classid`),
  KEY `fk_classledgers_2` (`ledgerid`),
  KEY `fk_tbl_classledgers_1_idx` (`createdby`),
  KEY `fk_tbl_classledgers_2_idx` (`modifedby`),
  CONSTRAINT `fk_classledgers_10` FOREIGN KEY (`classid`) REFERENCES `tbl_vouchertypeclass` (`classid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_classledgers_20` FOREIGN KEY (`ledgerid`) REFERENCES `tbl_ledger` (`ledgerid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_classledgers_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_classledgers_2` FOREIGN KEY (`modifedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_classledgers`
--

LOCK TABLES `tbl_classledgers` WRITE;
/*!40000 ALTER TABLE `tbl_classledgers` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_classledgers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_company`
--

DROP TABLE IF EXISTS `tbl_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_company` (
  `companyid` int(11) NOT NULL AUTO_INCREMENT,
  `directory` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `mailingname` varchar(100) NOT NULL,
  `address` varchar(250) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `pincode` varchar(20) DEFAULT NULL,
  `telephoneno` varchar(20) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `currencysymbol` varchar(3) NOT NULL,
  `financialyear` date NOT NULL,
  `booksbeginning` date NOT NULL,
  `administrator` int(11) DEFAULT NULL,
  `currencyname` varchar(45) NOT NULL,
  `decimalplaces` tinyint(4) NOT NULL,
  `symbolfordecimal` varchar(45) DEFAULT NULL,
  `amountsinmillions` tinyint(1) NOT NULL,
  `spacebtwamountandsymbol` tinyint(1) NOT NULL,
  `decimalplacsforprint` tinyint(4) NOT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL,
  `city` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`companyid`),
  KEY `fk_tbl_company_1_idx` (`createdby`),
  KEY `fk_tbl_company_2_idx` (`modifiedby`),
  CONSTRAINT `fk_tbl_company_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_company_2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_company`
--

LOCK TABLES `tbl_company` WRITE;
/*!40000 ALTER TABLE `tbl_company` DISABLE KEYS */;
INSERT INTO `tbl_company` VALUES (1,'','Arasan','arasan','s','India','Tamil Nadu','22','22','a@s.com','Rs.','0000-00-00','0000-00-00',5,'Rupees',2,'paise',0,0,0,5,'2014-07-16 18:08:40',NULL,NULL,1,'c'),(2,'','aaa','a','s','India','Tamil Nadu','34','34','a@a.com','Rs.','0000-00-00','0000-00-00',6,'Rupees',2,'paise',0,0,0,6,'2014-07-16 18:14:03',NULL,NULL,1,'c');
/*!40000 ALTER TABLE `tbl_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_group`
--

DROP TABLE IF EXISTS `tbl_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_group` (
  `groupid` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `grouplikesubledger` tinyint(1) DEFAULT NULL,
  `balancesforreporting` tinyint(1) DEFAULT NULL,
  `usedforcalc` tinyint(1) DEFAULT NULL,
  `methodwheninpurchaseinv` varchar(50) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `deletedon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deletedby` int(11) DEFAULT NULL,
  `predefined` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`groupid`),
  KEY `fk_group_1` (`companyid`),
  KEY `fk_tbl_group_1_idx` (`createdby`),
  KEY `fk_tbl_group_2_idx` (`modifiedby`),
  CONSTRAINT `fk_group_10` FOREIGN KEY (`companyid`) REFERENCES `tbl_company` (`companyid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_group_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_group_2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_group`
--

LOCK TABLES `tbl_group` WRITE;
/*!40000 ALTER TABLE `tbl_group` DISABLE KEYS */;
INSERT INTO `tbl_group` VALUES (1,1,'2014-07-16 18:08:40',1,NULL,NULL,'Branch / Divisions','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(2,1,'2014-07-16 18:08:40',1,NULL,NULL,'Current Assets','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(3,1,'2014-07-16 18:08:40',1,NULL,NULL,'Capital Account','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(4,1,'2014-07-16 18:08:40',1,NULL,NULL,'Current Liabilities','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(5,1,'2014-07-16 18:08:40',1,NULL,NULL,'Direct Expenses','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(6,1,'2014-07-16 18:08:40',1,NULL,NULL,'Direct Incomes','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(7,1,'2014-07-16 18:08:40',1,NULL,NULL,'Fixed Assets','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(8,1,'2014-07-16 18:08:40',1,NULL,NULL,'Indirect Expenses','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(9,1,'2014-07-16 18:08:40',1,NULL,NULL,'Indirect Incomes','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(10,1,'2014-07-16 18:08:40',1,NULL,NULL,'Investments','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(11,1,'2014-07-16 18:08:40',1,NULL,NULL,'Loans (Liability)','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(12,1,'2014-07-16 18:08:40',1,NULL,NULL,'Misc. Expenses (ASSET)','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(13,1,'2014-07-16 18:08:40',1,NULL,NULL,'Purchase Accounts','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(14,1,'2014-07-16 18:08:40',1,NULL,NULL,'Sales Accounts','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(15,1,'2014-07-16 18:08:40',1,NULL,NULL,'Suspense A/c','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(16,1,'2014-07-16 18:08:40',1,NULL,NULL,'Bank Accounts','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(17,1,'2014-07-16 18:08:40',1,NULL,NULL,'Bank OD A/c','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(18,1,'2014-07-16 18:08:40',1,NULL,NULL,'Cash-in-hand','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(19,1,'2014-07-16 18:08:40',1,NULL,NULL,'Deposits (Asset)','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(20,1,'2014-07-16 18:08:40',1,NULL,NULL,'Duties & Taxes','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(21,1,'2014-07-16 18:08:40',1,NULL,NULL,'Loans & Advances (Asset)','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(22,1,'2014-07-16 18:08:40',1,NULL,NULL,'Provisions','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(23,1,'2014-07-16 18:08:40',1,NULL,NULL,'Reserves & Surplus','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(24,1,'2014-07-16 18:08:40',1,NULL,NULL,'Secured Loans','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(25,1,'2014-07-16 18:08:40',1,NULL,NULL,'Stock-in-hand','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(26,1,'2014-07-16 18:08:40',1,NULL,NULL,'Sundry Creditors','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(27,1,'2014-07-16 18:08:40',1,NULL,NULL,'Sundry Debtors','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(28,1,'2014-07-16 18:08:40',1,NULL,NULL,'Unsecured Loans','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(29,2,'2014-07-16 18:14:03',1,NULL,NULL,'Branch / Divisions','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(30,2,'2014-07-16 18:14:03',1,NULL,NULL,'Current Assets','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(31,2,'2014-07-16 18:14:03',1,NULL,NULL,'Capital Account','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(32,2,'2014-07-16 18:14:03',1,NULL,NULL,'Direct Expenses','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(33,2,'2014-07-16 18:14:03',1,NULL,NULL,'Current Liabilities','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(34,2,'2014-07-16 18:14:03',1,NULL,NULL,'Direct Incomes','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(35,2,'2014-07-16 18:14:03',1,NULL,NULL,'Indirect Expenses','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(36,2,'2014-07-16 18:14:03',1,NULL,NULL,'Indirect Incomes','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(37,2,'2014-07-16 18:14:03',1,NULL,NULL,'Fixed Assets','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(38,2,'2014-07-16 18:14:03',1,NULL,NULL,'Investments','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(39,2,'2014-07-16 18:14:03',1,NULL,NULL,'Misc. Expenses (ASSET)','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(40,2,'2014-07-16 18:14:03',1,NULL,NULL,'Sales Accounts','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(41,2,'2014-07-16 18:14:03',1,NULL,NULL,'Purchase Accounts','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(42,2,'2014-07-16 18:14:03',1,NULL,NULL,'Loans (Liability)','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(43,2,'2014-07-16 18:14:03',1,NULL,NULL,'Bank Accounts','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(44,2,'2014-07-16 18:14:03',1,NULL,NULL,'Bank OD A/c','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(45,2,'2014-07-16 18:14:03',1,NULL,NULL,'Cash-in-hand','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(46,2,'2014-07-16 18:14:03',1,NULL,NULL,'Suspense A/c','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(47,2,'2014-07-16 18:14:03',1,NULL,NULL,'Deposits (Asset)','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(48,2,'2014-07-16 18:14:03',1,NULL,NULL,'Loans & Advances (Asset)','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(49,2,'2014-07-16 18:14:03',1,NULL,NULL,'Reserves & Surplus','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(50,2,'2014-07-16 18:14:03',1,NULL,NULL,'Secured Loans','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(51,2,'2014-07-16 18:14:03',1,NULL,NULL,'Provisions','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(52,2,'2014-07-16 18:14:03',1,NULL,NULL,'Duties & Taxes','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(53,2,'2014-07-16 18:14:03',1,NULL,NULL,'Stock-in-hand','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(54,2,'2014-07-16 18:14:03',1,NULL,NULL,'Sundry Creditors','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(55,2,'2014-07-16 18:14:03',1,NULL,NULL,'Unsecured Loans','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(56,2,'2014-07-16 18:14:03',1,NULL,NULL,'Sundry Debtors','',0,1,1,'None',0,'0000-00-00 00:00:00',NULL,0),(57,2,'2014-07-20 07:38:54',6,NULL,NULL,'test','testgrp',0,0,0,'Appropriate by Qty',0,'2014-07-20 07:38:54',NULL,0),(58,2,'2014-07-20 08:03:04',6,NULL,NULL,'Retest','retest',1,0,1,'Appropriate by Value',0,'2014-07-20 08:03:04',NULL,0),(59,2,'2014-08-03 08:48:25',6,'2014-08-03 08:48:49',6,'augtest','aug',1,1,0,'Appropriate by Qty',1,'2014-08-03 03:19:09',6,0);
/*!40000 ALTER TABLE `tbl_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_group_childdetails`
--

DROP TABLE IF EXISTS `tbl_group_childdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_group_childdetails` (
  `tbl_groupdetailsid` int(11) NOT NULL AUTO_INCREMENT,
  `groupid` int(11) DEFAULT NULL,
  `under` int(11) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  `deletedon` datetime NOT NULL,
  `deletedby` int(11) NOT NULL,
  PRIMARY KEY (`tbl_groupdetailsid`),
  KEY `fk_groupdetails_1` (`groupid`),
  KEY `fk_tbl_groupdetails_1_idx` (`createdby`),
  KEY `fk_tbl_groupdetails_2_idx` (`under`),
  KEY `fk_tbl_groupdetails_3_idx` (`modifiedby`),
  CONSTRAINT `fk_groupdetails_10` FOREIGN KEY (`groupid`) REFERENCES `tbl_group` (`groupid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_groupdetails_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_groupdetails_2` FOREIGN KEY (`under`) REFERENCES `tbl_group` (`groupid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_groupdetails_3` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_group_childdetails`
--

LOCK TABLES `tbl_group_childdetails` WRITE;
/*!40000 ALTER TABLE `tbl_group_childdetails` DISABLE KEYS */;
INSERT INTO `tbl_group_childdetails` VALUES (1,16,2,'2014-07-16 18:08:40',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(2,17,11,'2014-07-16 18:08:40',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(3,20,4,'2014-07-16 18:08:40',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(4,19,2,'2014-07-16 18:08:40',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(5,18,2,'2014-07-16 18:08:40',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(6,25,2,'2014-07-16 18:08:40',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(7,23,3,'2014-07-16 18:08:40',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(8,26,4,'2014-07-16 18:08:40',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(9,22,4,'2014-07-16 18:08:40',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(10,21,2,'2014-07-16 18:08:40',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(11,24,11,'2014-07-16 18:08:40',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(12,28,11,'2014-07-16 18:08:40',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(13,27,2,'2014-07-16 18:08:40',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(14,45,30,'2014-07-16 18:14:03',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(15,44,42,'2014-07-16 18:14:03',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(16,52,33,'2014-07-16 18:14:03',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(17,49,31,'2014-07-16 18:14:03',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(18,53,30,'2014-07-16 18:14:03',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(19,47,30,'2014-07-16 18:14:03',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(20,56,30,'2014-07-16 18:14:03',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(21,51,33,'2014-07-16 18:14:03',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(22,43,30,'2014-07-16 18:14:03',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(23,48,30,'2014-07-16 18:14:03',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(24,55,42,'2014-07-16 18:14:03',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(25,54,33,'2014-07-16 18:14:03',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(26,50,42,'2014-07-16 18:14:03',1,NULL,NULL,0,'0000-00-00 00:00:00',0),(27,57,30,'2014-07-20 07:38:54',6,NULL,NULL,0,'0000-00-00 00:00:00',0),(28,58,31,'2014-07-20 08:03:04',6,NULL,NULL,0,'0000-00-00 00:00:00',0),(29,59,34,'2014-08-03 08:48:25',6,6,'2014-08-03 08:48:49',1,'2014-08-03 08:49:09',6);
/*!40000 ALTER TABLE `tbl_group_childdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_inventoryvoucher`
--

DROP TABLE IF EXISTS `tbl_inventoryvoucher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_inventoryvoucher` (
  `inventoryvoucherid` int(11) NOT NULL,
  `vouchertype` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `reference` varchar(15) DEFAULT NULL,
  `party` int(11) DEFAULT NULL,
  `narration` varchar(300) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  PRIMARY KEY (`inventoryvoucherid`),
  KEY `fk_inventoryvoucher_1` (`vouchertype`),
  KEY `fk_inventoryvoucher_2` (`party`),
  KEY `fk_tbl_inventoryvoucher_1_idx` (`createdby`),
  KEY `fk_tbl_inventoryvoucher_2_idx` (`modifiedby`),
  CONSTRAINT `fk_inventoryvoucher_10` FOREIGN KEY (`vouchertype`) REFERENCES `tbl_vouchertype` (`voucherid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_inventoryvoucher_20` FOREIGN KEY (`party`) REFERENCES `tbl_ledger` (`ledgerid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_inventoryvoucher_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_inventoryvoucher_2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_inventoryvoucher`
--

LOCK TABLES `tbl_inventoryvoucher` WRITE;
/*!40000 ALTER TABLE `tbl_inventoryvoucher` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_inventoryvoucher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_inventoryvoucherend`
--

DROP TABLE IF EXISTS `tbl_inventoryvoucherend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_inventoryvoucherend` (
  `inventoryvoucherid` int(11) NOT NULL,
  `ledger` int(11) NOT NULL,
  `percent` decimal(10,0) DEFAULT NULL,
  `amount` decimal(10,0) NOT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  KEY `fk_inventoryvoucherend_1` (`inventoryvoucherid`),
  KEY `fk_inventoryvoucherend_2` (`ledger`),
  KEY `fk_tbl_inventoryvoucherend_1_idx` (`createdby`),
  KEY `fk_tbl_inventoryvoucherend_2_idx` (`modifiedby`),
  CONSTRAINT `fk_inventoryvoucherend_10` FOREIGN KEY (`inventoryvoucherid`) REFERENCES `tbl_inventoryvoucher` (`inventoryvoucherid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_inventoryvoucherend_20` FOREIGN KEY (`ledger`) REFERENCES `tbl_ledger` (`ledgerid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_inventoryvoucherend_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_inventoryvoucherend_2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_inventoryvoucherend`
--

LOCK TABLES `tbl_inventoryvoucherend` WRITE;
/*!40000 ALTER TABLE `tbl_inventoryvoucherend` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_inventoryvoucherend` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_inventoryvoucherid`
--

DROP TABLE IF EXISTS `tbl_inventoryvoucherid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_inventoryvoucherid` (
  `voucherid` int(11) NOT NULL,
  `inventoryvoucherid` varchar(15) NOT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  PRIMARY KEY (`voucherid`),
  KEY `fk_inventoryvoucherid_1` (`voucherid`),
  KEY `fk_tbl_inventoryvoucherid_1_idx` (`createdby`),
  KEY `fk_tbl_inventoryvoucherid_2_idx` (`modifiedby`),
  CONSTRAINT `fk_inventoryvoucherid_10` FOREIGN KEY (`voucherid`) REFERENCES `tbl_inventoryvoucher` (`inventoryvoucherid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_inventoryvoucherid_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_inventoryvoucherid_2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_inventoryvoucherid`
--

LOCK TABLES `tbl_inventoryvoucherid` WRITE;
/*!40000 ALTER TABLE `tbl_inventoryvoucherid` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_inventoryvoucherid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_inventoryvoucherlist`
--

DROP TABLE IF EXISTS `tbl_inventoryvoucherlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_inventoryvoucherlist` (
  `inventoryvoucherid` int(11) NOT NULL,
  `inventory` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `rate` decimal(10,0) NOT NULL,
  `discount` decimal(10,0) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifedon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  KEY `fk_inventoryvoucherlist_1` (`inventory`),
  KEY `fk_inventoryvoucherlist_2` (`inventoryvoucherid`),
  KEY `fk_tbl_inventoryvoucherlist_1_idx` (`createdby`),
  KEY `fk_tbl_inventoryvoucherlist_2_idx` (`modifiedby`),
  CONSTRAINT `fk_inventoryvoucherlist_10` FOREIGN KEY (`inventory`) REFERENCES `tbl_stockitem` (`stockitemid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_inventoryvoucherlist_20` FOREIGN KEY (`inventoryvoucherid`) REFERENCES `tbl_inventoryvoucher` (`inventoryvoucherid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_inventoryvoucherlist_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_inventoryvoucherlist_2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_inventoryvoucherlist`
--

LOCK TABLES `tbl_inventoryvoucherlist` WRITE;
/*!40000 ALTER TABLE `tbl_inventoryvoucherlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_inventoryvoucherlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_ledger`
--

DROP TABLE IF EXISTS `tbl_ledger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ledger` (
  `companyid` int(11) NOT NULL,
  `ledgerid` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `alias` varchar(10) DEFAULT NULL,
  `under` int(11) DEFAULT NULL,
  `maintainbillbybill` tinyint(1) NOT NULL,
  `address` varchar(250) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `pincode` varchar(30) DEFAULT NULL,
  `panno` varchar(30) DEFAULT NULL,
  `salestaxno` varchar(30) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  `deletedon` datetime NOT NULL,
  `deletedby` int(11) NOT NULL,
  PRIMARY KEY (`ledgerid`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `alias_UNIQUE` (`alias`),
  UNIQUE KEY `panno_UNIQUE` (`panno`),
  UNIQUE KEY `salestaxno_UNIQUE` (`salestaxno`),
  KEY `fk_ledger_1` (`companyid`),
  KEY `fk_ledger_2` (`under`),
  KEY `fk_tbl_ledger_1_idx` (`createdby`),
  KEY `fk_tbl_ledger_2_idx` (`modifiedby`),
  CONSTRAINT `fk_ledger_10` FOREIGN KEY (`companyid`) REFERENCES `tbl_company` (`companyid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ledger_20` FOREIGN KEY (`under`) REFERENCES `tbl_group` (`groupid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_ledger_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_ledger_2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_ledger`
--

LOCK TABLES `tbl_ledger` WRITE;
/*!40000 ALTER TABLE `tbl_ledger` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_ledger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_ledgerbalance`
--

DROP TABLE IF EXISTS `tbl_ledgerbalance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ledgerbalance` (
  `ledgerid` int(11) DEFAULT NULL,
  `openingbalance` double NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  `deletedon` datetime NOT NULL,
  `deletedby` int(11) NOT NULL,
  KEY `fk_ledgerbalance_1` (`ledgerid`),
  KEY `fk_tbl_ledgerbalance_1_idx` (`createdby`),
  KEY `fk_tbl_ledgerbalance_2_idx` (`modifiedby`),
  CONSTRAINT `fk_ledgerbalance_10` FOREIGN KEY (`ledgerid`) REFERENCES `tbl_ledger` (`ledgerid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_ledgerbalance_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_ledgerbalance_2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_ledgerbalance`
--

LOCK TABLES `tbl_ledgerbalance` WRITE;
/*!40000 ALTER TABLE `tbl_ledgerbalance` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_ledgerbalance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_license_company`
--

DROP TABLE IF EXISTS `tbl_license_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_license_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `companyid` int(11) NOT NULL,
  `companymappedon` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `companymappedby` int(11) NOT NULL,
  `purchaseid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_license_company`
--

LOCK TABLES `tbl_license_company` WRITE;
/*!40000 ALTER TABLE `tbl_license_company` DISABLE KEYS */;
INSERT INTO `tbl_license_company` VALUES (1,'2014-07-16 18:08:40',1,'0000-00-00 00:00:00',0,1),(2,'2014-07-16 18:14:03',2,'0000-00-00 00:00:00',0,5),(3,'2014-07-16 12:16:16',0,'0000-00-00 00:00:00',0,6),(4,'2014-07-16 12:43:19',0,'0000-00-00 00:00:00',0,7);
/*!40000 ALTER TABLE `tbl_license_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_license_purchase`
--

DROP TABLE IF EXISTS `tbl_license_purchase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_license_purchase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `licensenumber` varchar(255) NOT NULL,
  `noofuserlicense` int(11) NOT NULL,
  `noofcompanylicense` int(11) NOT NULL,
  `emailid` varchar(255) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createdby` int(11) NOT NULL,
  `modifiedon` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expireson` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_license_purchase`
--

LOCK TABLES `tbl_license_purchase` WRITE;
/*!40000 ALTER TABLE `tbl_license_purchase` DISABLE KEYS */;
INSERT INTO `tbl_license_purchase` VALUES (1,'null',2,0,'bharath@etpl.com','2014-06-29 08:08:03',1,'0000-00-00 00:00:00','2014-07-31 00:00:00'),(2,'2',0,0,'b@b.com','2014-07-13 06:55:57',0,'0000-00-00 00:00:00','2021-12-25 00:00:00'),(3,'3',0,0,'b@b.com','2014-07-13 06:55:57',0,'0000-00-00 00:00:00','2021-12-25 00:00:00'),(4,'4',1,1,'test@test.com','2014-07-15 11:36:08',1,'0000-00-00 00:00:00','2014-07-31 00:00:00'),(5,'5',1,1,'s@s.com','2014-07-16 12:08:38',1,'0000-00-00 00:00:00','2014-07-31 00:00:00'),(6,'6',0,1,'s@s.com','2014-07-16 12:16:16',1,'0000-00-00 00:00:00','2014-07-31 00:00:00'),(7,'7',1,1,'a@a.com','2014-07-16 12:43:19',1,'0000-00-00 00:00:00','2014-07-31 00:00:00');
/*!40000 ALTER TABLE `tbl_license_purchase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_license_user_changelog`
--

DROP TABLE IF EXISTS `tbl_license_user_changelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_license_user_changelog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userlicenseid` int(11) NOT NULL,
  `oldemailid` varchar(255) NOT NULL,
  `newemailid` varchar(255) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createdby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_license_user_changelog`
--

LOCK TABLES `tbl_license_user_changelog` WRITE;
/*!40000 ALTER TABLE `tbl_license_user_changelog` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_license_user_changelog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mapping_user_company`
--

DROP TABLE IF EXISTS `tbl_mapping_user_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mapping_user_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) DEFAULT NULL,
  `usermappedon` int(11) DEFAULT NULL,
  `usermappedby` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mapping_user_company`
--

LOCK TABLES `tbl_mapping_user_company` WRITE;
/*!40000 ALTER TABLE `tbl_mapping_user_company` DISABLE KEYS */;
INSERT INTO `tbl_mapping_user_company` VALUES (1,2,2147483647,6,6);
/*!40000 ALTER TABLE `tbl_mapping_user_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_stockcategory`
--

DROP TABLE IF EXISTS `tbl_stockcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_stockcategory` (
  `stockcategoryid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `alias` varchar(10) DEFAULT NULL,
  `companyid` int(11) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `deletedon` datetime NOT NULL,
  `deletedby` int(11) NOT NULL,
  PRIMARY KEY (`stockcategoryid`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `stockcategoryid` (`stockcategoryid`),
  UNIQUE KEY `alias_UNIQUE` (`alias`),
  KEY `fk_tbl_stockcategorydetails_1_idx` (`createdby`),
  KEY `fk_tbl_stockcategorydetails_2_idx` (`modifiedby`),
  KEY `fk_tbl_stockcategory_1_idx` (`companyid`),
  CONSTRAINT `fk_tbl_stockcategorydetails_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_stockcategorydetails_2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_stockcategory_1` FOREIGN KEY (`companyid`) REFERENCES `tbl_company` (`companyid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_stockcategory`
--

LOCK TABLES `tbl_stockcategory` WRITE;
/*!40000 ALTER TABLE `tbl_stockcategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_stockcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_stockcategory_under`
--

DROP TABLE IF EXISTS `tbl_stockcategory_under`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_stockcategory_under` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `stockcategory_prim` int(11) DEFAULT NULL,
  `stockcategory_under` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `deletedon` datetime NOT NULL,
  `deletedby` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_stockcategory_under`
--

LOCK TABLES `tbl_stockcategory_under` WRITE;
/*!40000 ALTER TABLE `tbl_stockcategory_under` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_stockcategory_under` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_stockgroup`
--

DROP TABLE IF EXISTS `tbl_stockgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_stockgroup` (
  `companyid` int(11) NOT NULL,
  `stockgroupid` int(11) NOT NULL AUTO_INCREMENT,
  `createdby` int(11) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `deletedon` datetime NOT NULL,
  `deletedby` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `quantity` tinyint(1) NOT NULL,
  PRIMARY KEY (`stockgroupid`),
  KEY `fk_stockgroup_1` (`companyid`),
  KEY `fk_tbl_stockgroup_1_idx` (`createdby`),
  KEY `fk_tbl_stockgroup_2_idx` (`modifiedby`),
  CONSTRAINT `fk_stockgroup_10` FOREIGN KEY (`companyid`) REFERENCES `tbl_company` (`companyid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_stockgroup_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_stockgroup_2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_stockgroup`
--

LOCK TABLES `tbl_stockgroup` WRITE;
/*!40000 ALTER TABLE `tbl_stockgroup` DISABLE KEYS */;
INSERT INTO `tbl_stockgroup` VALUES (2,1,6,'2014-08-03 14:27:41',NULL,NULL,0,'0000-00-00 00:00:00',0,'test','atest',0),(2,2,6,'2014-08-03 14:32:02',NULL,NULL,0,'0000-00-00 00:00:00',0,'test2','t2',0),(2,3,6,'2014-08-03 14:33:27',NULL,NULL,0,'0000-00-00 00:00:00',0,'test3','t3',0),(2,4,6,'2014-08-03 14:35:04',NULL,NULL,0,'0000-00-00 00:00:00',0,'test4','t4',1);
/*!40000 ALTER TABLE `tbl_stockgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_stockgroupdetails`
--

DROP TABLE IF EXISTS `tbl_stockgroupdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_stockgroupdetails` (
  `stockgroupid` int(11) NOT NULL,
  `under` int(11) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `modfiedby` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `deletedon` datetime NOT NULL,
  `deletedby` int(11) NOT NULL,
  `tbl_stockgroupdetailsid` int(11) NOT NULL,
  KEY `fk_stockgroupdetails_1` (`stockgroupid`),
  KEY `fk_stockgroupdetails_2` (`under`),
  KEY `fk_tbl_stockgroupdetails_1_idx` (`createdby`),
  KEY `fk_tbl_stockgroupdetails_2_idx` (`modfiedby`),
  CONSTRAINT `fk_stockgroupdetails_10` FOREIGN KEY (`stockgroupid`) REFERENCES `tbl_stockgroup` (`stockgroupid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_stockgroupdetails_20` FOREIGN KEY (`under`) REFERENCES `tbl_stockgroup` (`stockgroupid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_stockgroupdetails_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_stockgroupdetails_2` FOREIGN KEY (`modfiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_stockgroupdetails`
--

LOCK TABLES `tbl_stockgroupdetails` WRITE;
/*!40000 ALTER TABLE `tbl_stockgroupdetails` DISABLE KEYS */;
INSERT INTO `tbl_stockgroupdetails` VALUES (1,1,'2014-08-03 14:29:24',6,NULL,NULL,0,'0000-00-00 00:00:00',0,0),(2,1,'2014-08-03 14:32:02',6,NULL,NULL,0,'0000-00-00 00:00:00',0,0),(3,1,'2014-08-03 14:33:27',6,NULL,NULL,0,'0000-00-00 00:00:00',0,0),(4,3,'2014-08-03 14:35:04',6,NULL,NULL,0,'0000-00-00 00:00:00',0,0);
/*!40000 ALTER TABLE `tbl_stockgroupdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_stockitem`
--

DROP TABLE IF EXISTS `tbl_stockitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_stockitem` (
  `companyid` int(11) DEFAULT NULL,
  `stockitemid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `alias` varchar(10) DEFAULT NULL,
  `under` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `unit` int(11) DEFAULT NULL,
  `tariffclassification` int(11) DEFAULT NULL,
  `alterexcise` tinyint(1) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  `deletedon` datetime NOT NULL,
  `deletedby` int(11) NOT NULL,
  PRIMARY KEY (`stockitemid`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `alias_UNIQUE` (`alias`),
  KEY `fk_stockitem_1` (`companyid`),
  KEY `fk_stockitem_2` (`under`),
  KEY `fk_stockitem_3` (`category`),
  KEY `fk_stockitem_4` (`tariffclassification`),
  KEY `fk_stockitem_5` (`unit`),
  KEY `fk_tbl_stockitem_1_idx` (`createdby`),
  KEY `fk_tbl_stockitem_2_idx` (`modifiedby`),
  CONSTRAINT `fk_stockitem_10` FOREIGN KEY (`companyid`) REFERENCES `tbl_company` (`companyid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_stockitem_20` FOREIGN KEY (`under`) REFERENCES `tbl_stockgroup` (`stockgroupid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_stockitem_30` FOREIGN KEY (`category`) REFERENCES `tbl_stockcategory_under` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_stockitem_40` FOREIGN KEY (`tariffclassification`) REFERENCES `tbl_tariffclassification` (`tariffid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_stockitem_50` FOREIGN KEY (`unit`) REFERENCES `tbl_unitsofmeasure` (`unitid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_stockitem_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_stockitem_2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_stockitem`
--

LOCK TABLES `tbl_stockitem` WRITE;
/*!40000 ALTER TABLE `tbl_stockitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_stockitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_stockitembalance`
--

DROP TABLE IF EXISTS `tbl_stockitembalance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_stockitembalance` (
  `stockitemid` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `balancedate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  `deletedon` datetime NOT NULL,
  `deletedby` int(11) NOT NULL,
  KEY `fk_stockitembalance_1` (`stockitemid`),
  KEY `fk_tbl_stockitembalance_1_idx` (`createdby`),
  KEY `fk_tbl_stockitembalance_2_idx` (`modifiedby`),
  CONSTRAINT `fk_stockitembalance_10` FOREIGN KEY (`stockitemid`) REFERENCES `tbl_stockitem` (`stockitemid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_stockitembalance_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_stockitembalance_2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_stockitembalance`
--

LOCK TABLES `tbl_stockitembalance` WRITE;
/*!40000 ALTER TABLE `tbl_stockitembalance` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_stockitembalance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tariffclassification`
--

DROP TABLE IF EXISTS `tbl_tariffclassification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tariffclassification` (
  `companyid` int(11) NOT NULL,
  `tariffid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `alias` varchar(10) DEFAULT NULL,
  `HSCcode` int(11) DEFAULT NULL,
  `usedfor` varchar(45) DEFAULT NULL,
  `notes` varchar(100) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`tariffid`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `alias_UNIQUE` (`alias`),
  KEY `fk_tariffclassification_1` (`companyid`),
  KEY `fk_tbl_tariffclassification_1_idx` (`createdby`),
  KEY `fk_tbl_tariffclassification_2_idx` (`modifiedby`),
  CONSTRAINT `fk_tariffclassification_10` FOREIGN KEY (`companyid`) REFERENCES `tbl_company` (`companyid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_tariffclassification_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_tariffclassification_2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tariffclassification`
--

LOCK TABLES `tbl_tariffclassification` WRITE;
/*!40000 ALTER TABLE `tbl_tariffclassification` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_tariffclassification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_unitsofmeasure`
--

DROP TABLE IF EXISTS `tbl_unitsofmeasure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_unitsofmeasure` (
  `companyid` int(11) NOT NULL,
  `unitid` int(11) NOT NULL AUTO_INCREMENT,
  `symbol` varchar(3) DEFAULT NULL,
  `formalname` varchar(45) DEFAULT NULL,
  `decimalplaces` int(11) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deletedon` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deletedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`unitid`),
  KEY `fk_unitsofmeasure_1` (`companyid`),
  KEY `fk_tbl_unitsofmeasure_1_idx` (`createdby`),
  KEY `fk_tbl_unitsofmeasure_2_idx` (`modifiedby`),
  CONSTRAINT `fk_tbl_unitsofmeasure_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_unitsofmeasure_2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_unitsofmeasure_10` FOREIGN KEY (`companyid`) REFERENCES `tbl_company` (`companyid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_unitsofmeasure`
--

LOCK TABLES `tbl_unitsofmeasure` WRITE;
/*!40000 ALTER TABLE `tbl_unitsofmeasure` DISABLE KEYS */;
INSERT INTO `tbl_unitsofmeasure` VALUES (2,1,'Rs','Rupees',2,6,'2014-08-06 16:37:20',NULL,NULL,0,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `tbl_unitsofmeasure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_unitsofmeasure_childdetails`
--

DROP TABLE IF EXISTS `tbl_unitsofmeasure_childdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_unitsofmeasure_childdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstunitid` int(11) DEFAULT NULL,
  `secondunitid` int(11) DEFAULT NULL,
  `conversion` float DEFAULT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createdby` int(11) DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(1) DEFAULT '0',
  `deletedon` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deletedby` int(11) DEFAULT NULL,
  `companyid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_unitsofmeasure_childdetails`
--

LOCK TABLES `tbl_unitsofmeasure_childdetails` WRITE;
/*!40000 ALTER TABLE `tbl_unitsofmeasure_childdetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_unitsofmeasure_childdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_userlogin_log`
--

DROP TABLE IF EXISTS `tbl_userlogin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_userlogin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `startedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `endedon` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `isalive` tinyint(1) NOT NULL,
  `connectionid` varchar(255) NOT NULL,
  `userid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_userlogin_log`
--

LOCK TABLES `tbl_userlogin_log` WRITE;
/*!40000 ALTER TABLE `tbl_userlogin_log` DISABLE KEYS */;
INSERT INTO `tbl_userlogin_log` VALUES (1,'2014-06-17 15:57:49','0000-00-00 00:00:00',0,'Xeet--ypGOg8NpS7cpDf',1),(2,'2014-06-19 18:12:14','0000-00-00 00:00:00',0,'RlNVuYmGgL0WXOY5BrnT',1),(3,'2014-06-19 18:30:24','0000-00-00 00:00:00',0,'VYmPfBy3qfghFZsuWzag',1),(4,'2014-06-29 06:29:33','0000-00-00 00:00:00',0,'OYEJcWogu_YbL_vWZev6',1),(5,'2014-06-29 06:35:56','0000-00-00 00:00:00',0,'zKqC0INmacHzez1nTwrz',1),(6,'2014-06-29 08:28:14','0000-00-00 00:00:00',0,'GpafC3nl7lHE5Z2ITwr1',1),(7,'2014-06-29 08:30:32','0000-00-00 00:00:00',0,'z9j5QI8uliDE5dfGu4i0',1),(8,'2014-06-29 08:32:40','0000-00-00 00:00:00',0,'nbeL6lpO7WTXpphcvvyX',1),(9,'2014-06-29 08:35:48','0000-00-00 00:00:00',0,'Xsg8y76z0AVBDrvKwPvK',1),(10,'2014-06-29 08:38:33','0000-00-00 00:00:00',0,'_Fbi_pU678eFCpFEw-Y1',1),(11,'2014-06-29 08:39:52','0000-00-00 00:00:00',0,'cOyZlKuB2W0NDspxxnYC',1),(12,'2014-06-29 08:44:59','2014-06-29 03:14:59',0,'O3w9lsI1GDiIgMTDx5Bu',1),(13,'2014-06-29 09:12:45','2014-06-29 03:42:45',0,'hxO93MW3-JytVzQ75W-s',1),(14,'2014-06-29 09:14:15','2014-06-29 03:44:15',0,'hxO93MW3-JytVzQ75W-s',1),(15,'2014-06-29 10:34:29','0000-00-00 00:00:00',0,'IOWGv4n9yYpmn7ePKQQp',1),(16,'2014-06-29 10:35:44','0000-00-00 00:00:00',0,'TcM8Ou_-IaaM8UbxKQQq',1),(17,'2014-06-29 10:37:30','0000-00-00 00:00:00',0,'MYImEH3c1nKx8VsbKQQr',1),(18,'2014-06-29 10:38:13','0000-00-00 00:00:00',0,'B05-TIAWw45KUxAwKQQs',1),(19,'2014-06-29 10:39:30','0000-00-00 00:00:00',0,'mLzQPSOoEF9oQKMzKQQt',1),(20,'2014-06-29 10:42:16','0000-00-00 00:00:00',0,'pSiOMGsUb-Pm-JDVKQQu',1),(21,'2014-06-29 11:00:56','0000-00-00 00:00:00',0,'wu2YpdZ63EVOE7YlKQQv',1),(22,'2014-06-29 11:03:05','0000-00-00 00:00:00',0,'iUbL22z2a6i1EKeSSLJB',1),(23,'2014-06-29 11:08:44','0000-00-00 00:00:00',0,'4TkMJ91mM1noruN3SwYX',1),(24,'2014-06-29 11:12:39','0000-00-00 00:00:00',0,'TxmNerKhLs339__pT-R8',1),(25,'2014-06-29 11:14:13','0000-00-00 00:00:00',0,'Qen3skRm2hTp7g3qU5hd',1),(26,'2014-06-29 11:31:53','0000-00-00 00:00:00',0,'vBF8cnSnoAxIc2RkVNxJ',1),(27,'2014-06-29 11:34:56','0000-00-00 00:00:00',0,'eRw3bjM3IGzuI-IAZSkd',1),(28,'2014-06-29 11:39:42','0000-00-00 00:00:00',0,'_l0Az-C4lqKkV4RAZ_rp',1),(29,'2014-06-29 12:37:53','0000-00-00 00:00:00',0,'b5VvV8QzBZ21TSc7bDf6',1),(30,'2014-06-29 12:40:16','0000-00-00 00:00:00',0,'I-pU1ld5d58gUlDpoUJU',1),(31,'2014-06-29 12:41:57','0000-00-00 00:00:00',0,'uIPBTxBkuZ0MmdqPo28w',1),(32,'2014-06-29 12:56:06','0000-00-00 00:00:00',0,'EY2wtIlcjNsufcSzpVae',1),(33,'2014-06-29 12:58:29','0000-00-00 00:00:00',0,'A4nfa3tPBmvq4-s4sfTo',1),(34,'2014-06-29 13:02:02','0000-00-00 00:00:00',0,'-8slCzgVOUxYvlRhtZ0l',1),(35,'2014-06-29 13:04:48','0000-00-00 00:00:00',0,'l9mn91TVQcalmmHDt7Ox',1),(36,'2014-06-29 13:06:10','0000-00-00 00:00:00',0,'6L37RANz272AENpjt7Oy',1),(37,'2014-06-29 13:06:59','0000-00-00 00:00:00',0,'LG33TeZrtiNNwOaCu2sG',1),(38,'2014-06-29 13:15:35','0000-00-00 00:00:00',0,'8-leMJlw84XRF55tu2sH',1),(39,'2014-06-29 13:28:18','0000-00-00 00:00:00',0,'JekkGiWSPQc4oY6hxISl',1),(40,'2014-06-29 13:29:44','0000-00-00 00:00:00',0,'TKcNNYK3vX4YL9l8xISm',1),(41,'2014-06-29 13:30:47','0000-00-00 00:00:00',0,'70l3gXD7MGlDyzYE0UCG',1),(42,'2014-06-29 13:31:53','0000-00-00 00:00:00',0,'ovoltOCuipE6Yvi40UCH',1),(43,'2014-06-29 13:37:17','0000-00-00 00:00:00',0,'SGMhw-ceu8QTQv-40UCJ',1),(44,'2014-06-29 14:03:17','0000-00-00 00:00:00',0,'VVFaiyRS81PdhDU22Edq',1),(45,'2014-06-29 16:21:51','0000-00-00 00:00:00',0,'7h9YgZoEgR-q5X4tbfzq',1),(46,'2014-06-29 16:22:45','2014-06-29 10:52:45',0,'VpvPrvn0bsceNfcgbfzr',1),(47,'2014-06-29 16:23:59','2014-06-29 10:53:59',0,'VpvPrvn0bsceNfcgbfzr',1),(48,'2014-07-01 12:24:51','2014-07-01 06:54:51',0,'CKE9_-tEzqf6tv_E4axG',1),(49,'2014-07-01 12:27:25','2014-07-01 06:57:25',0,'uHI4p1rfq-yU2zPZ4axH',4),(50,'2014-07-02 12:15:38','0000-00-00 00:00:00',0,'lfc2Sb84VRqYV1VVZKzJ',1),(51,'2014-07-02 12:18:45','2014-07-02 06:48:45',0,'_eW_8RZaaEmB89zF_MCV',1),(52,'2014-07-02 12:20:20','2014-07-02 06:50:20',0,'_eW_8RZaaEmB89zF_MCV',4),(53,'2014-07-03 09:46:17','2014-07-03 04:16:17',0,'4sCUBLw50nXlwk7qnB5g',4),(54,'2014-07-04 05:21:41','2014-07-03 23:51:41',0,'MQgaiBlu0cp-mGFJz_U3',4),(55,'2014-07-05 10:31:15','2014-07-05 05:01:15',0,'HCc9bCpmDZcNqQ1NAxle',1),(56,'2014-07-05 10:31:52','2014-07-05 05:01:52',0,'5AjUVQRvM_aD_iNGAxlf',4),(57,'2014-07-05 10:33:23','2014-07-05 05:03:23',0,'5AjUVQRvM_aD_iNGAxlf',4),(58,'2014-07-05 10:33:20','2014-07-05 05:03:20',0,'HCc9bCpmDZcNqQ1NAxle',1),(59,'2014-07-16 17:13:51','2014-07-16 11:43:51',0,'EvPhqZ4xZQk3PsYFrgHP',1),(60,'2014-07-16 17:13:51','2014-07-16 11:43:51',0,'EvPhqZ4xZQk3PsYFrgHP',1),(61,'2014-07-16 17:13:51','2014-07-16 11:43:51',0,'v5iCKVUWdCdL7HuK-c8z',1),(62,'2014-07-16 17:13:51','2014-07-16 11:43:51',0,'--_dt_EUELnJiYpQKam7',1),(63,'2014-07-16 18:12:55','2014-07-16 12:17:21',0,'s7YBqFgkvoUr6NwvYG4X',1),(64,'2014-07-16 18:12:55','2014-07-16 12:20:08',0,'s7YBqFgkvoUr6NwvYG4X',1),(65,'2014-07-16 18:12:57','2014-07-16 12:42:57',0,'s7YBqFgkvoUr6NwvYG4X',5),(66,'2014-07-16 18:13:24','2014-07-16 12:43:24',0,'s7YBqFgkvoUr6NwvYG4X',1),(67,'2014-07-16 18:35:50','2014-07-16 13:05:50',0,'WqRCzZ5L65r3ciR_YG4Y',6),(68,'2014-07-17 04:33:55','2014-07-16 23:03:55',0,'CvP5_iNUWVh0-CFYmBU_',6),(69,'2014-07-17 18:16:39','0000-00-00 00:00:00',0,'Z0Tj3AXWQHdTUGWbZsd3',6),(70,'2014-07-17 18:09:00','2014-07-17 12:39:00',0,'o-vjGekdugsqILZGc1SB',4),(71,'2014-07-17 18:15:28','2014-07-17 12:45:28',0,'_N9P8ixfHvyrL_bxiAaO',4),(72,'2014-07-17 18:22:42','2014-07-17 12:52:42',0,'MlII0vbpVarom3qyiddb',6),(73,'2014-07-23 17:24:25','0000-00-00 00:00:00',0,'HpobB9N4ndTiU2pOyuv9',6),(74,'2014-07-23 18:45:36','2014-07-23 13:15:36',0,'LIFUARq25ZpRFqW3itda',6),(75,'2014-07-24 19:06:50','2014-07-24 13:36:50',0,'-WOG8DFiL8gkfsCVtyZW',6),(76,'2014-07-25 07:15:11','2014-07-25 01:45:11',0,'CSye3p2-O0W_sPPJXTMB',6),(77,'2014-07-25 07:23:17','2014-07-25 01:53:17',0,'lvjnwW_7TCtnT2-0YRz0',6),(78,'2014-07-25 09:32:09','2014-07-25 04:02:09',0,'OGhJoZB7y3kR4Myz3GZJ',6),(79,'2014-07-25 09:33:58','2014-07-25 04:03:58',0,'b6lEGyaKooAFDTnh3GZL',6),(80,'2014-07-25 09:45:40','2014-07-25 04:15:40',0,'RQ_g6WqsOXnA2FXX3GZM',6),(81,'2014-07-25 09:49:21','2014-07-25 04:19:21',0,'daWlb7pEK4fZpcdV6cFX',6),(82,'2014-07-28 03:23:57','2014-07-27 21:53:57',0,'QdcwMcBW1FSQ-gg0_dwV',6),(83,'2014-07-30 18:33:31','2014-07-30 13:03:31',0,'gMjhpJkgV6ANXpSeiyMv',6),(84,'2014-08-03 10:19:08','2014-08-03 04:49:08',0,'LYMpYqPVZihXoJa9YSy-',6),(85,'2014-08-03 11:22:50','2014-08-03 05:52:50',0,'eEntVh0eE21ZrgLbZMXB',6),(86,'2014-08-03 18:30:47','2014-08-03 13:00:47',0,'berQ1f01xi5vk1nrlg-c',6),(87,'2014-08-06 16:36:36','0000-00-00 00:00:00',0,'uwaKG0jUHaSQkO1SPRgI',6),(88,'2014-08-06 16:46:59','0000-00-00 00:00:00',1,'lMxKKv5bqax5MWzRN2zJ',6);
/*!40000 ALTER TABLE `tbl_userlogin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_userpasswordchange`
--

DROP TABLE IF EXISTS `tbl_userpasswordchange`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_userpasswordchange` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `oldpassword` varchar(255) NOT NULL,
  `newpassword` varchar(255) NOT NULL,
  `changedon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_userpasswordchange`
--

LOCK TABLES `tbl_userpasswordchange` WRITE;
/*!40000 ALTER TABLE `tbl_userpasswordchange` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_userpasswordchange` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_userpermissions`
--

DROP TABLE IF EXISTS `tbl_userpermissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_userpermissions` (
  `idtbl_userpermissions` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `permissionallowed` text,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `companyid` int(11) NOT NULL,
  PRIMARY KEY (`idtbl_userpermissions`),
  KEY `fk_tbl_userpermissions_1_idx` (`userid`),
  KEY `fk_tbl_userpermissions_2_idx` (`createdby`),
  KEY `fk_tbl_userpermissions_3_idx` (`modifiedby`),
  CONSTRAINT `fk_tbl_userpermissions_1` FOREIGN KEY (`userid`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_userpermissions_2` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_userpermissions_3` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_userpermissions`
--

LOCK TABLES `tbl_userpermissions` WRITE;
/*!40000 ALTER TABLE `tbl_userpermissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_userpermissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_users` (
  `idtbl_users` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `issuperuser` tinyint(1) NOT NULL,
  `createdon` datetime NOT NULL,
  `isactive` tinyint(1) NOT NULL,
  `useremail` varchar(255) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `expireson` datetime DEFAULT NULL,
  PRIMARY KEY (`idtbl_users`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_users`
--

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;
INSERT INTO `tbl_users` VALUES (1,'su','Sonamuthu','5f4dcc3b5aa765d61d8327deb882cf99',1,'2014-06-16 00:00:00',1,NULL,NULL,NULL,NULL),(4,'bharath@etpl.com','bharath@etpl.com','5f4dcc3b5aa765d61d8327deb882cf99',0,'2014-06-29 13:38:03',1,NULL,NULL,NULL,NULL),(5,'s@s.com',NULL,'5f4dcc3b5aa765d61d8327deb882cf99',0,'2014-07-16 17:38:38',1,'s@s.com',5,1,NULL),(6,'a@a.com',NULL,'5f4dcc3b5aa765d61d8327deb882cf99',0,'2014-07-16 18:13:19',1,'a@a.com',7,1,NULL);
/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_vouchertype`
--

DROP TABLE IF EXISTS `tbl_vouchertype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_vouchertype` (
  `companyid` int(11) NOT NULL,
  `voucherid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `alias` varchar(10) DEFAULT NULL,
  `typeofvoucher` varchar(100) NOT NULL,
  `abbreviation` varchar(10) DEFAULT NULL,
  `methodofvouchering` varchar(100) NOT NULL,
  `preventduplicates` tinyint(1) DEFAULT NULL,
  `advanceconfiguration` tinyint(1) DEFAULT NULL,
  `startingnumber` int(11) DEFAULT NULL,
  `widthofnumericalpart` int(11) DEFAULT NULL,
  `prefillwithzero` tinyint(1) DEFAULT NULL,
  `effectivedates` tinyint(1) NOT NULL,
  `optionaldefault` tinyint(1) NOT NULL,
  `commonnarration` tinyint(1) NOT NULL,
  `narrationforeachentry` tinyint(1) NOT NULL,
  `printaftersave` tinyint(1) NOT NULL,
  `printreceiptaftersave` tinyint(1) NOT NULL,
  `classid` int(11) DEFAULT NULL,
  `stockinventory` tinyint(1) NOT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`voucherid`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `alias_UNIQUE` (`alias`),
  KEY `fk_vouchertype_1` (`companyid`),
  KEY `fk_vouchertype_2` (`classid`),
  KEY `fk_tbl_vouchertype_1_idx` (`createdby`),
  KEY `fk_tbl_vouchertype_2_idx` (`modifiedby`),
  CONSTRAINT `fk_tbl_vouchertype_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_vouchertype_2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_vouchertype_10` FOREIGN KEY (`companyid`) REFERENCES `tbl_company` (`companyid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_vouchertype_20` FOREIGN KEY (`classid`) REFERENCES `tbl_vouchertypeclass` (`classid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_vouchertype`
--

LOCK TABLES `tbl_vouchertype` WRITE;
/*!40000 ALTER TABLE `tbl_vouchertype` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_vouchertype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_vouchertypeclass`
--

DROP TABLE IF EXISTS `tbl_vouchertypeclass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_vouchertypeclass` (
  `companyid` int(11) NOT NULL,
  `classid` int(11) NOT NULL,
  `nameofclass` varchar(45) NOT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  PRIMARY KEY (`classid`),
  UNIQUE KEY `nameofclass_UNIQUE` (`nameofclass`),
  KEY `fk_vouchertypeclass_1` (`companyid`),
  KEY `fk_tbl_vouchertypeclass_1_idx` (`createdby`),
  KEY `fk_tbl_vouchertypeclass_2_idx` (`modifiedby`),
  CONSTRAINT `fk_tbl_vouchertypeclass_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_vouchertypeclass_2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_vouchertypeclass_10` FOREIGN KEY (`companyid`) REFERENCES `tbl_company` (`companyid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_vouchertypeclass`
--

LOCK TABLES `tbl_vouchertypeclass` WRITE;
/*!40000 ALTER TABLE `tbl_vouchertypeclass` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_vouchertypeclass` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_vouchertypeprefix`
--

DROP TABLE IF EXISTS `tbl_vouchertypeprefix`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_vouchertypeprefix` (
  `voucherid` int(11) NOT NULL,
  `applicablefrom` date NOT NULL,
  `particulars` varchar(10) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  KEY `fk_vouchertypeprefix_1` (`voucherid`),
  KEY `fk_tbl_vouchertypeprefix_1_idx` (`createdby`),
  KEY `fk_tbl_vouchertypeprefix_2_idx` (`modifiedby`),
  CONSTRAINT `fk_tbl_vouchertypeprefix_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_vouchertypeprefix_2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_vouchertypeprefix_10` FOREIGN KEY (`voucherid`) REFERENCES `tbl_vouchertype` (`voucherid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_vouchertypeprefix`
--

LOCK TABLES `tbl_vouchertypeprefix` WRITE;
/*!40000 ALTER TABLE `tbl_vouchertypeprefix` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_vouchertypeprefix` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_vouchertyperestart`
--

DROP TABLE IF EXISTS `tbl_vouchertyperestart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_vouchertyperestart` (
  `voucherid` int(11) NOT NULL,
  `applicablefrom` date NOT NULL,
  `startingnumber` int(11) NOT NULL,
  `particulars` varchar(10) NOT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  KEY `fk_vouchertyperestart_1` (`voucherid`),
  KEY `fk_tbl_vouchertyperestart_1_idx` (`createdby`),
  KEY `fk_tbl_vouchertyperestart_2_idx` (`modifiedby`),
  CONSTRAINT `fk_tbl_vouchertyperestart_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_vouchertyperestart_2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_vouchertyperestart_10` FOREIGN KEY (`voucherid`) REFERENCES `tbl_vouchertype` (`voucherid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_vouchertyperestart`
--

LOCK TABLES `tbl_vouchertyperestart` WRITE;
/*!40000 ALTER TABLE `tbl_vouchertyperestart` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_vouchertyperestart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_vouchertypsuffix`
--

DROP TABLE IF EXISTS `tbl_vouchertypsuffix`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_vouchertypsuffix` (
  `voucherid` int(11) NOT NULL,
  `applicablefrom` date NOT NULL,
  `particulars` varchar(10) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  KEY `fk_vouchertypsuffix_1` (`voucherid`),
  KEY `fk_tbl_vouchertypsuffix_1_idx` (`createdby`),
  KEY `fk_tbl_vouchertypsuffix_2_idx` (`modifiedby`),
  CONSTRAINT `fk_tbl_vouchertypsuffix_1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_vouchertypsuffix_2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`idtbl_users`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_vouchertypsuffix_10` FOREIGN KEY (`voucherid`) REFERENCES `tbl_vouchertype` (`voucherid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_vouchertypsuffix`
--

LOCK TABLES `tbl_vouchertypsuffix` WRITE;
/*!40000 ALTER TABLE `tbl_vouchertypsuffix` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_vouchertypsuffix` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-06 22:17:36
