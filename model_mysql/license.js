var execquery = require('./executequery');

exports.getuserlicense = function getuserlicense(connid,callback){
	var sql="select idtbl_users,issuperuser,username from tbl_users tu join tbl_userlogin_log tul on tu.idtbl_users=tul.userid "+
	" where tul.connectionid='"+connid+"' order by startedat desc limit 1";
	execquery.executeselectquery(sql,function(user_data){
		if(user_data.error!=null){
			callback(execquery.buildjson("getuserlicense","Error in finding usertype",user_data.error));
			return;
		}
		console.log('User License '+JSON.stringify(user_data));
		if(user_data.data[0].issuperuser==0){
			var sql="select u.*,tlp.emailid purchasedby,tlp.expireson expirydate from tbl_users u join "+
			"tbl_license_purchase tlp on u.purchase_id=tlp.id where tlp.emailid='"+user_data.data[0].username+"'";
		}
		if(user_data.data[0].issuperuser==1)
			var sql="select u.*,tlp.emailid purchasedby,tlp.expireson expirydate from tbl_users u join "+
			"tbl_license_purchase tlp on u.purchase_id=tlp.id";
		execquery.executeselectquery(sql,function(user_licesne_data){
			if(user_licesne_data.error!=null){
				callback(execquery.buildjson("getuserlicense","Error in select query",user_licesne_data.error));
				return;
			}
			callback(execquery.buildjson("getuserlicense",{"user_license":user_licesne_data.data},null));
		});
	});
}

exports.getcompanylicense = function getcompanylicense(connid,callback){
	var sql="select idtbl_users,issuperuser,username from tbl_users tu join tbl_userlogin_log tul on tu.idtbl_users=tul.userid "+
	" where tul.connectionid='"+connid+"' order by startedat desc limit 1";
	execquery.executeselectquery(sql,function(user_data){
		if(user_data.error!=null){
			callback(execquery.buildjson("getcompanylicense","Error in finding usertype",user_data.error));
			return;
		}
		if(user_data.data[0].issuperuser==0){
			var sql="select tlc.*,tc.name companyname,tlp.expireson expirydate,tlp.emailid purchasedby from "+
			"tbl_license_company tlc join tbl_license_purchase tlp on tlc.purchaseid=tlp.id left outer join "+
			"tbl_company tc on tc.companyid=tlc.companyid where tlp.emailid='"+user_data.data[0].username+"'";
		}
		if(user_data.data[0].issuperuser==1)
			var sql="select tlc.*,tc.name companyname,tpl.expireson expirydate,tpl.emailid purchasedby from "+
			" tbl_license_company tlc left outer join tbl_company tc on tc.companyid=tlc.companyid join "+
			" tbl_license_purchase tpl on tpl.id=tlc.purchaseid";
		execquery.executeselectquery(sql,function(company_licesne_data){
			if(company_licesne_data.error!=null){
				callback(execquery.buildjson("getcompanylicense","Error in select query",company_licesne_data.error));
				return;
			}
			callback(execquery.buildjson("getcompanylicense",{"company_license":company_licesne_data.data},null));
		});
	});
}

exports.has_empty_companylicense_slot = function has_empty_companylicense_slot(user_email,callback){
	var sql="SELECT count(*) cnt,tlc.id,tlp.id purchaseid FROM tbl_license_company tlc join tbl_license_purchase tlp on "+
	"tlp.id=tlc.purchaseid where tlp.emailid='"+user_email+"' and tlc.companyid=0";
	execquery.executeselectquery(sql,function(has_cmplicense){
		if(has_cmplicense.error!=null){
			callback(execquery.buildjson("has_empty_companylicense_slot","Error in finding empty cmp slot",has_cmplicense.error));
			return;
		}
		if(has_cmplicense.data[0].cnt==0){
			callback(execquery.buildjson("has_empty_companylicense_slot",{"availability":false},null));
			return;
		}
		callback(execquery.buildjson("has_empty_companylicense_slot",{"availability":true,"id":
		has_cmplicense.data[0].id,"purchaseid":has_cmplicense.data[0].purchaseid},null));
	});
};

exports.unuseduserlicense = function unuseduserlicense(useremail,callback){
	var sql="select count(*) cnt,idtbl_users id from tbl_users where purchase_id=(select purchase_id from tbl_users "+
	"where username='"+useremail+"' limit 1) and username!='' limit 1";
	execquery.executeselectquery(sql,function(unused_user_license){
		if(unused_user_license.error!=null){
			callback(execquery.buildjson("unuseduserlicense","error in select qry",unused_user_license.error));
			return;
		}
		
		callback(execquery.buildjson("unuseduserlicense",{"count":unused_user_license.data[0].cnt,
		"empty_slot_id":unused_user_license.data[0].id},null));
	});
};
