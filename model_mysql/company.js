var execquery = require('./executequery');
var common = require('./commonfunc');
var myssqlsufunc = require('./sufunc');

exports.createcompany = function createcompany(companydetails,license_id,user_id,purchase_id,user_email,callback){
	console.log('From Create Company '+JSON.stringify(companydetails));
	console.log(typeof companydetails);
	//companydetails = JSON.parse(companydetails);
	var sql="select count(*) cnt from tbl_company where name='"+companydetails.companyname+"'";
	execquery.executeselectquery(sql,function(company_data){
		if(company_data.error!=null){
			callback(execquery.buildjson("createcompany","Error in Cmp finding qry",company_data.error));
			return;
		}
		if(company_data.data[0].cnt>0){
			callback(execquery.buildjson("createcompany","Company Already Exists",null));
			return;
		}
		var sql="insert into tbl_company(createdon,createdby,isactive,name,mailingname,financialyear,address,city,"+
		"pincode,telephoneno,country,state,email,currencyname,currencysymbol,decimalplaces,symbolfordecimal,"+
		"amountsinmillions,administrator) values(UTC_TIMESTAMP(),'"+user_id+"',1,'"+companydetails.companyname+
		"','"+companydetails.alias+"','"+companydetails.financialyearstartfrom+"','"+companydetails.street+
		"','"+companydetails.city+"','"+companydetails.pincode+"','"+companydetails.telephone
		+"','"+companydetails.country+"','"+companydetails.state+"','"+companydetails.email+"','"+companydetails.currencyname+
		"','"+companydetails.currencysymbol+"','"+companydetails.decimalplaces+"','"+companydetails.symbolfordecimal+
		"','"+companydetails.showinmillions+"','"+user_id+"')";
		execquery.executequery(sql,function(cmp_insert_data){
		if(cmp_insert_data.error!=null){
			callback(execquery.buildjson("createcompany","Error in Cmp Insertion",cmp_insert_data.error));
			return;
		}
		
		common.addingdatatocompany(cmp_insert_data.data.insertId);
		var sql="update tbl_license_company set companyid='"+cmp_insert_data.data.insertId+"' where id='"+license_id+"'";
		execquery.executequery(sql,function(cmp_update_data){
		if(cmp_update_data.error!=null){
			callback(execquery.buildjson("createcompany","Error in Cmp Insertion",cmp_update_data.error));
			return;
		}

		var sql="insert into tbl_mapping_user_company(companyid,user_id,usermappedon,usermappedby) VALUES "+
		"('"+cmp_insert_data.data.insertId+"',(select idtbl_users from tbl_users where useremail='"+user_email
		+"' limit 1),UTC_TIMESTAMP(),'"+user_id+"')";
		execquery.executequery(sql,function(update_userlic_data){
			if(update_userlic_data.error!=null){
				callback(execquery.buildjson("createcompany","Error in User Lics Updation",update_userlic_data.error));
				return;
			}
			callback(execquery.buildjson("createcompany","Company Created Successfully",null)); 
		});
		});
	});
});
};

exports.getcompanydetails = function getcompanydetails(companyid,connid,callback){
	//check if user is associated with the company
	var sql="select count(*) cnt from tbl_mapping_user_company mp join tbl_userlogin_log l on mp.user_id=l.userid "+
 "where l.connectionid='"+connid+"' and isalive=1 and mp.companyid='"+companyid+"' order by startedat limit 1";
		execquery.executeselectquery(sql,function(companyaccessscheckdata){
			if(companyaccessscheckdata.error!=null){
				callback(execquery.buildjson("getcompanydetails","Error in checking User access on company",companyaccessscheckdata.error));
				return;
				}
			if(companyaccessscheckdata.data[0].cnt<1){
				callback(execquery.buildjson("getcompanydetails","Company Not Accesible by User",null));
				return;
				}
			//get details of company
			var sql="select name companyname,mailingname alias,financialyear financialyearfrom,"+
			"address street,city,pincode,telephoneno telephone,country,state,email,currencyname,currencysymbol,"+
			"decimalplaces,symbolfordecimal,amountsinmillions showinmillions from tbl_company where companyid="+companyid;
		execquery.executequery(sql,function(cmp_select_data){
		if(cmp_select_data.error!=null){
			callback(execquery.buildjson("getcompanydetails","Error in getting company details",cmp_select_data.error));
			return;
		}	
			callback(execquery.buildjson("getcompanydetails",cmp_select_data.data,null));
		});
	});
}


exports.insertpermission = function insertpermission(admin_id,userid,companyid,callback){
	var rights = JSON.stringify({"groups":"xrxx","ledgers":"xrxx","vouchertypes":"xrxx","inventoryinfo":"xrxx",
	"stockgroups":"xrxx","stockcategories":"xrxx","stockitems":"xrxx","unitsofmeasure":"xrxx","excise":"xrxx",
	"excisefordealer":"xrxx","valueaddedtax":"xrxx","taxdeductedatsource":"xrxx","taxcollectedatsource":"xrxx",
	"servicetax":"xrxx","accountingvouchers":"xrxx","inventoryvouchers":"xrxx","ordervouchers":"xrxx",
	"importofdata":"xrxx","banking":"xrxx","trialbalance":"xrxx","daybook":"xrxx","accountsbook":"xrxx",
	"statementsofaccounts":"xrxx","inventorybooks":"xrxx","statementofinventory":"xrxx","cash/fundsflow":"xrxx",
	"receiptsandpayments":"xrxx","listofaccounts":"xrxx","exceptionreports":"xrxx","balancesheet":"xrxx",
	"profit&lossA/c":"xrxx","stocksummary":"xrxx","ratioanalysis":"xrxx"});
	
	var sql="insert into tbl_userpermissions(userid,permissionallowed,createdon,createdby,companyid)"+
	" values('"+userid+"','"+rights+"',UTC_TIMESTAMP(),'"+admin_id+"','"+companyid+"')";
	execquery.executequery(sql,function(perm_data){
		if(perm_data.error!=null){
			callback(execquery.buildjson("insertpermission","Error in Perm Insertion",perm_data.error));
			return;
		}
		callback(execquery.buildjson("insertpermission","Perm Inserted",null));
	});
}; 


exports.updatecompany = function updatecompany(companyid,companydetails,user_id,callback){
	var sql="update tbl_company set name='"+companydetails.companyname+"',mailingname='"+companydetails.alias+
	"',financialyear='"+companydetails.financialyearstartfrom+"',address'"+companydetails.street+"',city='"+
	companydetails.city+"',pincode='"+companydetails.pincode+"',telephoneno='"+companydetails.telephone
	+"',country='"+companydetails.country+"',state='"+companydetails.state+"',email='"+companydetails.email
	+"',currencyname='"+companydetails.currencyname+"',currencysymbol='"+companydetails.currencysymbol
	+"',decimalplaces='"+companydetails.decimalplaces+"',symbolfordecimal='"+companydetails.symbolfordecimal
	+"',amountsinmillions='"+companydetails.showinmillions+"',modifiedby='"+user_id+"',modifiedon=UTC_TIMESTAMP()"+
	"where companyid="+companyid;
	execquery.executequery(sql,function(upd_data){
		if(upd_data.error!=null){
			callback(execquery.buildjson("updatecompany","Error in Update qry",upd_data.error));
			return;
		}
		callback(execquery.buildjson("updatecompany",upd_data.data[0].affectedRows,null));
	});
};


exports.getusermappeddetails = function getusermappeddetails(companyid,callback){
	var sql="select u.useremail from tbl_mapping_user_company mp join tbl_users u on mp.user_id=u.userid where companyid='"+companyid+"'";
	execquery.executeselectquery(sql,function(user_mapped){
		if(user_mapped.error!=null){
			callback(execquery.buildjson("getusermappeddetails","Error in select qry",user_mapped.error));
			return;
		}
		callback(execquery.buildjson("getusermappeddetails",user_mapped.data,null));
	});
};

exports.addusertocompany = function addusertocompany(admin_id,new_user_email,companyid,user_licene_id,callback){
	
	var sql="insert into tbl_mapping_user_company(companyid,usermappedon,usermappedby,user_id) values "+
	"('"+companyid+"',UTC_TIMESTAMP(),'"+admin_id+"',(select userid from tbl_users where useremail='"+user_email+"' limit 1))";
	execquery.executequery(sql,function(upd_data){
		if(upd_data.error!=null){
			callback(execquery.buildjson("addusertocompany","Error Updating tbl_mapping_user_company",upd_data.error));
			return;
		}
		mysqlsufunc.createuser(new_user_email,function(new_user_data){
			if(new_user_data.response.error!=null){
				callback(execquery.buildjson("addusertocompany","Error in create user",new_user_data.response.error));
				return;
			}
			exports.insertpermission(admin_id,new_user_data.response.msg.user_id,companyid,function(perm_data){
				if(perm_data.response.error!=null){
					callback(execquery.buildjson("addusertocompany","Error in Insert Perm",perm_data.response.error));
					return;
				}
				callback(execquery.buildjson("addusertocompany","User Added To Company",null));
			});
		});
	});
};
