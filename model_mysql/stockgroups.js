var execquery = require('./executequery');

exports.getall_stockgroups = function getall_stockgroups(companyid,callback){
	var sql="select tsg.stockgroupid,tsg.name,tsg.alias,tsg.quantity,tsgc.under understockgroupid,(select name from "+
	" tbl_stockgroup where stockgroupid=tsgc.under) understockgrpname from tbl_stockgroup tsg left outer join "+
	" tbl_stockgroupdetails tsgc on tsg.stockgroupid=tsgc.stockgroupid where companyid='"+companyid+
	"' and tsg.deleted=0 and (tsgc.deleted=0 or  tsgc.deleted IS NULL)";
	execquery.executeselectquery(sql,function(all_stockgroup_data){
	if(all_stockgroup_data.error!=null){
		callback(execquery.buildjson("getall_stockgroups","Error in select qry",all_stockgroup_data.error));
		return;
	}
	callback(execquery.buildjson("getall_stockgroups",all_stockgroup_data.data,null));
	});
};

exports.getdetails_stockgroups = function getdetails_stockgroups(companyid,stockgroupid,callback){
	var sql="select tsg.stockgroupid,tsg.name,tsg.alias,tsg.quantity,tsgc.under understockgroupid from tbl_stockgroup "+
	" tsg left outer join tbl_stockgroupdetails tsgc on tsg.stockgroupid=tsgc.stockgroupid where companyid='"+companyid+
	"' and tsg.stockgroupid='"+stockgroupid+"'";
	execquery.executeselectquery(sql,function(stockgroup_data){
	if(stockgroup_data.error!=null){
		callback(execquery.buildjson("getdetails_stockgroups","Error in select qry",stockgroup_data.error));
		return;
	}
	callback(execquery.buildjson("getdetails_stockgroups",stockgroup_data.data,null));
	});
}; 

exports.create_stockgroups = function create_stockgroups(user_id,companyid,stockgroupdata,callback){
	var quantity = (stockgroupdata.qty_add=='yes')?1:0;
	var stockgroupid = null;
	var sql="insert into tbl_stockgroup(companyid,name,alias,quantity,createdon,createdby) values('"+companyid+
	"','"+stockgroupdata.name+"','"+stockgroupdata.alias+"','"+quantity+"',UTC_TIMESTAMP(),'"+user_id+"')";
	execquery.executequery(sql,function(stk_grp_data){
		if(stk_grp_data.error!=null){
			callback(execquery.buildjson("create_stockgroups","Error in tbl_grp insertion",stk_grp_data.error));
			return;
		}
		stockgroupid = stk_grp_data.data.insertId;
		var subtblflag = false;
		if(stockgroupdata.under.length>0 && stockgroupdata.under!='')
		{
			subtblflag = true;
			var sql="insert into tbl_stockgroupdetails(stockgroupid,under,createdon,createdby) values('"+
			stockgroupid+"','"+stockgroupdata.under+"',UTC_TIMESTAMP(),'"+user_id+"')";
			execquery.executequery(sql,function(stk_grp_sub_data){
				if(stk_grp_sub_data.error!=null){
					callback(execquery.buildjson("create_stockgroups","Error in tbl_grp_childdetails insertion",stk_grp_sub_data.error));
					return;
				}
				callback(execquery.buildjson("create_stockgroups",{"msg":"Stock Group Created Successfully","stockgroupid":stockgroupid},null));
			});
		}
		if(subtblflag===false)
			callback(execquery.buildjson("create_stockgroups",{"msg":"Stock Group Created Successfully","stockgroupid":stockgroupid},null));
	});
};

exports.update_stockgroups = function update_stockgroups(user_id,companyid,stk_grpid,stockgroupdata,callback){
	var sql="update tbl_stockgroup tsg join tbl_stockgroupdetails tsgd on tsg.stockgroupid=tsgd.stockgroupid set"+
	" tsg.modifiedon=UTC_TIMESTAMP(),tsg.modifiedby='"+user_id+"',tsg.name='"+stockgroupdata.name+"',tsg.alias='"+
	stockgroupdata.alias+"',tsg.quantity='"+stockgroupdata.quantity+"'";
	if(stockgroupdata.under.length>0 && stockgroupdata.under!='')
	{
		sql+=" ,tsgd.under='"+stockgroupdata.under+"',tsgd.modifiedon=UTC_TIMESTAMP(),tsgd.modifiedby='"+user_id+"'"; 
	}
	sql+=" where tsg.stockgroupid='"+stk_grpid+"' and tsg.companyid='"+companyid+"'";
	execquery.executequery(sql,function(stk_grp_update_data){
		if(stk_grp_update_data.error!=null){
			callback(execquery.buildjson("update_stockgroups","Error in Upd Qry",stk_grp_update_data.error));
			return;
		}
		callback(execquery.buildjson("update_stockgroups","Stock Groups Update Successfull",null));
	});
	
};

exports.delete_stockgroups = function delete_stockgroups(stk_groupid,user_id,companyid,callback){
	var sql="update tbl_stockgroup tsg left outer join tbl_stockgroupdetails tsgd on tsg.stockgroupid=tsgd.stockgroupid set "+
	" tsg.deletedon=UTC_TIMESTAMP(),tsg.deletedby='"+user_id+"',tsg.deleted=1,tsgd.deletedon=UTC_TIMESTAMP(),"+
	" tsgd.deletedby='"+user_id+"',tsgd.deleted=1 where groupid='"+stk_groupid+"' and tsg.companyid='"+companyid+"'";
	execquery.executequery(sql,function(stk_grp_del_data){
		if(stk_grp_del_data.error!=null){
			callback(execquery.buildjson("delete_stockgroups","Error in Delete",stk_grp_del_data.error));
			return;
		}
		callback(execquery.buildjson("delete_stockgroups","Stock Grp Deleted Successfully",null));
	});
};
