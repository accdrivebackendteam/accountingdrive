var execquery = require('./executequery');

exports.su_createlicense = function su_createlicense(connid,user_id,email,price,user,expireson,company,callback){

var sql="select count(*) cnt from tbl_users where username='"+email+"' and isactive=1";
execquery.executeselectquery(sql,function(empty_slot_data){
	if(empty_slot_data.error!=null){
		ob = {"licensecreated":false,"msg":"Error in retrieving empty slot"};
		callback(execquery.buildjson("createlicense",ob,empty_slot_data.error));
		return;
	}
	var exist_cnt = empty_slot_data.data[0].cnt;
	if(exist_cnt==0 && company>0 && user==0){
		ob = {"licensecreated":false,"msg":"You need a user license before getting a company license"};
		callback(execquery.buildjson("createlicense",ob,null));
		return;
	}

	var sql="select max(id)+1 licenseno from tbl_license_purchase";
	execquery.executeselectquery(sql,function(max_data){
		if(max_data.error!=null){
			ob = {"licensecreated":false,"msg":"Error in max qry"};
			callback(execquery.buildjson("createlicense",ob,max_data.error));
			return;
		}
	
		var licenseno = max_data.data[0].licenseno;
		var sql = "insert into tbl_license_purchase(licensenumber,noofuserlicense,noofcompanylicense,emailid,"+
		"createdon,createdby,expireson) values('"+licenseno+"','"+user+"','"+company+"','"+email
		+"',UTC_TIMESTAMP(),'"+user_id+"','"+expireson+"')";
		execquery.executequery(sql,function(insert_license){
			if(insert_license.error!=null)
			{
				ob={"licensecreated":false,"msg":"License insert qry"};
				callback(execquery.buildjson("createlicense",ob,insert_license.error));
				return;
			}
			var license_id = insert_license.data.insertId;
			var companyflag = (company>0)?false:true;
			var userflag = (user>0)?false:true;
			var ob = '';
			if(company>0){
				insert_company_license(company,license_id,function(cmp_inst_data){
					companyflag = true;
					ob = cmp_inst_data;
				});
			}
			if(user>0){
				insert_user_license(user,user_id,email,license_id,exist_cnt,function(usr_inst_data){
					userflag = true;
					ob = usr_inst_data;
				});
			}
			if(companyflag==true && userflag==true){
				callback(execquery.buildjson("su_createlicense",ob,ob.error));
			}
			});
		});
	});
};

function insert_company_license(company,license_id,callback){
	for(var i=0;i<company;i++){
		var sql="insert into tbl_license_company(createdon,purchaseid) values(UTC_TIMESTAMP(),'"+
		license_id+"')";
		execquery.executequery(sql,function(insert_company_license){
			if(insert_company_license.error!=null)
			{
			ob = {"licensecreated":false,"msg":"Cmpy License insert qry","error":insert_company_license.error};
			callback(ob);
			return;
			}
		});
		if(i+1==company){
			ob = {"licensecreated":true,"msg":"Cmpy License insert qry","error":null};
			callback(ob);
		}
	}
};

function insert_user_license(user,user_id,email,license_id,exist_cnt,callback){
	for(var i=0;i<user;i++){
		if(i==0 && exist_cnt==0){
			var sql="insert into tbl_users(createdon,createdby,username,useremail,password,"+
			"issuperuser,purchase_id,isactive) values(UTC_TIMESTAMP(),'"+user_id+"','"+email+"','"+
			email+"',MD5('password'),0,'"+license_id+"',1)";
		}
		else{
			var sql="insert into tbl_users(purchase_id,issuperuser,isactive) values('"+license_id+
			"',0,0)";
		}
		execquery.executequery(sql,function(insert_user_license){
			if(insert_user_license.error!=null)
			{
				ob = {"licensecreated":false,"msg":"Cmpy License insert qry","error":insert_user_license.error};
				callback(ob);
				return;
			}
		});
		if(i+1==user){
			ob = {"licensecreated":true,"msg":"Cmpy License insert qry","error":null};
			callback(ob);
		}
	}
};

	exports.createuser = function createuser(slot_id,email,user_id,callback){
		var sql="select count(*) cnt,idtbl_users user_id from tbl_users where username='"+email+"'";
		execquery.executeselectquery(sql,function(user_data){
			if(user_data.error!=null){
				callback(execquery.buildjson("createuser","Error in Create User qry",user_data.error));
				return;
			}
			if(user_data.data[0].cnt==1){
				callback(execquery.buildjson("createuser","User already Exists",null));
				return;
			}
			//var passwd = exports.generatepasswd();
			var sql="update tbl_users set createdon=UTC_TIMESTAMP(),createby='"+user_id+"',username='"+email+"',password="+
			"MD5('password'),issuperuser=0,isactive=1 where idtbl_users='"+slot_id+"'";
			execquery.executequery(sql,function(new_user_data){
				if(new_user_data.error!=null){
					callback(execquery.buildjson("createuser","Error in update qry",new_user_data.error));
					return;
				}
				if(new_user_data.data[0].affectedRows==0){
					callback(execquery.buildjson("createuser","Creation failed",null));
					return;
				}
				callback(execquery.buildjson("createuser","Creation Successfull",null));
			});
		});
};

exports.checkif_having_userlicense = function checkif_having_userlicense(useremail,callback){
	var sql="select count(*) cnt from tbl_users where username='"+email+"' and isactive=1";
	execquery.executeselectquery(sql,function(empty_slot_data){
		if(empty_slot_data.error!=null){
			callback(e)
		}
	});
};
