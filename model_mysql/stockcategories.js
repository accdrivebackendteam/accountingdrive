var execquery = require('./executequery');

exports.getall_stockcategories = function getall_stockcategories(companyid,callback){
	var sql="select scg.stockcategoryid,scg.name,scg.alias,scgc.stockcategory_under understockcategory,(select name "+
	" from tbl_stockcategory where stockcategoryid=stockcategory_under) understockctgname from tbl_stockcategory scg "+
	" left outer join tbl_stockcategory_under scgc on scg.stockcategoryid=scgc.stockcategory_prim where companyid='"+
	companyid+"' and scg.deleted=0 and (scgc.deleted=0 or  scgc.deleted IS NULL)";
	execquery.executeselectquery(sql,function(all_stockcatg_data){
	if(all_stockcatg_data.error!=null){
		callback(execquery.buildjson("getall_stockcategories","Error in select qry",all_stockcatg_data.error));
		return;
	}
	callback(execquery.buildjson("getall_stockcategories",all_stockcatg_data.data,null));
	});
};

exports.getdetails_stockcategories = function getdetails_stockcategories(stockcatgid,companyid,callback){
	var sql="select scg.stockcategoryid,scg.name,scg.alias,scgc.stockcategory_under understockcategoryid from "+
	" tbl_stockcategory scg join tbl_stockcategory_under scgc on scg.stockcategoryid=scgc.stockcategory_prim where companyid='"+
	companyid+"' and scg.stockcategoryid='"+stockcatgid+"'";
	execquery.executeselectquery(sql,function(stockcatg_data){
	if(stockcatg_data.error!=null){
		callback(execquery.buildjson("getdetails_stockcategories","Error in select qry",stockcatg_data.error));
		return;
	}
	callback(execquery.buildjson("getdetails_stockcategories",stockcatg_data.data,null));
	});
}; 

exports.create_stockcategories = function create_stockcategories(user_id,companyid,stockcatgdata,callback){
	var stockcatgid = null;
	var sql="insert into tbl_stockcategory(companyid,name,alias,createdon,createdby) values('"+companyid+
	"','"+stockcatgdata.name+"','"+stockcatgdata.alias+"',UTC_TIMESTAMP(),'"+user_id+"')";
	execquery.executequery(sql,function(stk_catg_data){
		if(stk_catg_data.error!=null){
			callback(execquery.buildjson("create_stockcategories","Error in tbl_grp insertion",stk_catg_data.error));
			return;
		}
		stockcatgid = stk_catg_data.data.insertId;
		var subtblflag = false;
		if(stockcatgdata.under.length>0 && stockcatgdata.under!='')
		{
			subtblflag = true;
			var sql="insert into tbl_stockcategory_under(stockcategory_prim,stockcategory_under,createdon,createdby) values('"+
			stockcatgid+"','"+stockcatgdata.under+"',UTC_TIMESTAMP(),'"+user_id+"')";
			execquery.executequery(sql,function(stk_catg_sub_data){
				if(stk_catg_sub_data.error!=null){
					callback(execquery.buildjson("create_stockcategories","Error in tbl_grp_childdetails insertion",stk_catg_sub_data.error));
					return;
				}
				callback(execquery.buildjson("create_stockcategories",{"msg":"Stock Category Created Successfully","stockcatgid":stockcatgid},null));
			});
		}
		if(subtblflag===false)
			callback(execquery.buildjson("create_stockcategories",{"msg":"Stock Category Created Successfully","stockcatgid":stockcatgid},null));
	});
};

exports.update_stockcategories = function update_stockcategories(user_id,stk_catgid,companyid,stockcatgdata,callback){
	var sql="update tbl_stockcategory scg join tbl_stockcategory_under scgc on scg.stockcategoryid=scgc."+
	" stockcategory_prim set scg.modifiedon=UTC_TIMESTAMP(),scg.modifiedby='"+user_id+"',scg.name='"+stockcatgdata.name+
	"',scg.alias='"+stockcatgdata.alias+"'";
	if(stockcatgdata.under.length>0 && stockcatgdata.under!='')
	{
		sql+=" ,scgc.stockcategory_under='"+stockcatgdata.under+"',scgc.modifiedon=UTC_TIMESTAMP(),scgc.modifiedby='"+
		user_id+"' ";
	}
	sql+=" where scg.stockcategoryid='"+stk_catgid+"' and scg.companyid='"+companyid+"'";
	execquery.executequery(sql,function(stk_catg_update_data){
		if(stk_catg_update_data.error!=null){
			callback(execquery.buildjson("update_stockcategories","Error in Upd Qry",stk_catg_update_data.error));
			return;
		}
		callback(execquery.buildjson("update_stockcategories","Stock Groups Update Successfull",null));
	});
	
};

exports.delete_stockcategories = function delete_stockcategories(stk_catgid,user_id,companyid,callback){
	var sql="update tbl_stockcategory scg left outer join tbl_stockcategory_under scgd on scg.stockcategoryid=scgd.stockcategory_prim "+
	" set scg.deletedon=UTC_TIMESTAMP(),scg.deletedby='"+user_id+"',scg.deleted=1,scgd.deletedon=UTC_TIMESTAMP(),"+
	" scgd.deletedby='"+user_id+"',scgd.deleted=1 where scg.stockcategoryid='"+stk_catgid+"' and scg.companyid='"+companyid+"'";
	execquery.executequery(sql,function(stk_catg_del_data){
		if(stk_catg_del_data.error!=null){
			callback(execquery.buildjson("delete_stockcategories","Error in Delete",stk_catg_del_data.error));
			return;
		}
		callback(execquery.buildjson("delete_stockcategories","Stock Grp Deleted Successfully",null));
	});
};
