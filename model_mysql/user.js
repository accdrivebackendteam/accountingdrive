var execquery = require('./executequery');

exports.checklogin = function checklogin(email,password,ipaddress,connid,callback){
	var sql="select count(*) cnt,idtbl_users,issuperuser from tbl_users where username='"+email+
	"' and MD5('"+password+"')=password and isactive=1";//should be >1
	execquery.executeselectquery(sql,function(user_data){
		if(user_data.error!=null)
		{
			callback(execquery.buildjson("login","Username,Password query",user_data.error));
			return;
		}
		if(user_data.data[0].cnt==0)
		{
			callback(execquery.buildjson("login",{"loggedin":false,"resp":"Error In Username or Password"},null));
			return;
		}
		
		var id = user_data.data[0].idtbl_users;
		var usertype = (user_data.data[0].issuperuser==0)?'normal':'su';
		var sql="select count(*) cnt from tbl_userlogin_log where userid='"+id+"' and isalive=1"; //sould be 0;	
		execquery.executeselectquery(sql,function(login_data){
			if(login_data.error!=null)
			{
				callback(execquery.buildjson("login","Already Loggedin query",login_data.error));
				return;
			}
			if(login_data.data[0].cnt==1)
			{
				callback(execquery.buildjson("login",{"loggedin":false,"resp":"User Already Logged In"},null));
				return;
			}
			var sql="insert into tbl_userlogin_log(startedat,isalive,connectionid,userid) values (UTC_TIMESTAMP(),1,'"+
			connid+"','"+id+"')";
			execquery.executequery(sql,function(insert_data){
				if(insert_data.error!=null)
				{
					callback(execquery.buildjson("login","Username,Password query",insert_data.error));
					return;
				}
				var sql="select count(*) cnt from tbl_license_company lc join tbl_license_purchase pl on "+
				"lc.purchaseid=pl.id where  emailid='"+email+"'";
				execquery.executequery(sql,function(license_data){
					if(license_data.error!=null)
					{
						callback(execquery.buildjson("login","License query",license_data.error));
						return;
					}
					console.log('License Data '+JSON.stringify(license_data));
	
					var ob = {"loggedin":true,"connid":connid,"usertype":usertype,"noofcompanylicense":
					license_data.data[0].cnt,"user_id":id};
					callback(execquery.buildjson("login",ob,null));
				});
			});
		});
	});
};

exports.getassociatedcompanylist = function getassociatedcompanylist(connid,callback){
	var sql="select c.companyid,c.name,(case when c.administrator=u.idtbl_users then 1 else 0 end) isadmin from tbl_mapping_user_company mp  join tbl_company c on mp.companyid=c.companyid join tbl_users u on u.idtbl_users=mp.user_id join tbl_userlogin_log l on l.userid=u.idtbl_users  where l.connectionid='"+connid+"' and isalive=1 order by startedat";
	execquery.executequery(sql,function(company_data){
		if(company_data.error!=null)
		{
			callback(execquery.buildjson("getassociatedcompanylist","Associated Company query",company_data.error));
			return;
		}
		var ob = {"associatedcompanylist":company_data.data};
		callback(execquery.buildjson("getassociatedcompanylist",ob,null));
	});
};

exports.logout = function logout(connid,user_id,callback){
	var sql="update tbl_userlogin_log set isalive=0,endedon=UTC_TIMESTAMP() WHERE userid='"+user_id+"' and isalive=1;";
	execquery.executequery(sql,function(logout_data){
	if(logout_data.error!=null)
		callback(execquery.buildjson("logout","Logout Qry Error",logout_data.error));
	else
		callback(execquery.buildjson("logout","Logged Off Successfully",null));
	});
};

exports.forcelogout = function forcelogout(email,callback){
	var sql="update tbl_userlogin_log tul join tbl_users tu on tul.userid=tu.idtbl_users set isalive=0"+
	",endedon=UTC_TIMESTAMP() WHERE tu.username='"+email+"' and isalive=1";
	execquery.executequery(sql,function(force_logout_data){
	if(force_logout_data.error!=null)
		callback(execquery.buildjson("forcelogout","Logout Qry Error",force_logout_data.error));
	else
		callback(execquery.buildjson("forcelogout","Logged Off Successfully",null));
	});
};

