var execquery = require('./executequery');

exports.getall_group = function getall_group(companyid,callback){
	var sql="select tg.groupid,tg.predefined,tg.name,tgc.tbl_groupdetailsid,tgc.under undergroupid, "+
	"(select name from tbl_group where groupid=tgc.under) undergroupname from tbl_group tg "+
	" left outer join tbl_group_childdetails tgc on tg.groupid=tgc.groupid where companyid='"+companyid+
	"' and tg.deleted=0";
	execquery.executeselectquery(sql,function(all_group_data){
	if(all_group_data.error!=null){
		callback(execquery.buildjson("getall_group","Error in select qry",all_group_data.error));
		return;
	}
	callback(execquery.buildjson("getall_group",all_group_data.data,null));
	});
};

exports.getdetails_group = function getdetails_group(groupid,companyid,callback){
	var sql="select tg.name,tg.alias,tg.grouplikesubledger,tg.balancesforreporting,tg.usedforcalc,"+
	"tg.methodwheninpurchaseinv,tgc.under from tbl_group tg join tbl_group_childdetails tgc on tg.groupid=tgc.groupid "+
	" where tg.groupid='"+groupid+"' and tg.companyid='"+companyid+"' and tg.deleted=0";
	execquery.executeselectquery(sql,function(group_data){
	if(group_data.error!=null){
		callback(execquery.buildjson("getdetails_group","Error in select qry",group_data.error));
		return;
	}
	callback(execquery.buildjson("getdetails_group",group_data.data,null));
	});
}; 

exports.create_group = function create_group(user_id,companyid,groupdata,callback){
	var subledger = (groupdata.subledger=='yes')?1:0;
	var reporting = (groupdata.reporting=='yes')?1:0;
	var calculation = (groupdata.calculation=='yes')?1:0;
	var groupid = null;
	var sql="insert into tbl_group(companyid,createdon,createdby,name,alias,grouplikesubledger,balancesforreporting,"+
	"usedforcalc,methodwheninpurchaseinv,predefined) values('"+companyid+"',UTC_TIMESTAMP(),'"+user_id+"','"+groupdata.name+
	"','"+groupdata.alias+"','"+subledger+"','"+reporting+"','"+calculation+"','"+groupdata.allocation+"',0)";
	execquery.executequery(sql,function(grp_data){
		if(grp_data.error!=null){
			callback(execquery.buildjson("create_group","Error in tbl_grp insertion",grp_data.error));
			return;
		}
		groupid = grp_data.data.insertId;
		var sql="insert into tbl_group_childdetails(groupid,under,createdon,createdby) values('"+groupid+
		"','"+groupdata.under+"',UTC_TIMESTAMP(),'"+user_id+"')";
		execquery.executequery(sql,function(grp_sub_data){
			if(grp_sub_data.error!=null){
				callback(execquery.buildjson("create_group","Error in tbl_grp_childdetails insertion",grp_sub_data.error));
				return;
			}
			callback(execquery.buildjson("create_group",{"msg":"Group Created Successfully","groupid":groupid},null));
		});
	});
};

exports.update_group = function update_group(user_id,groupdata,companyid,groupid,callback){
	var subledger = (groupdata.subledger=='yes')?1:0;
	var reporting = (groupdata.reporting=='yes')?1:0;
	var calculation = (groupdata.calculation=='yes')?1:0;
	var sql="update tbl_group tg join tbl_group_childdetails tgc on tg.groupid=tgc.groupid set tg.modifiedon="+
	" UTC_TIMESTAMP(),tg.modifiedby='"+user_id+"',tg.name='"+groupdata.name+"',tg.alias='"+groupdata.alias+
	"',tg.grouplikesubledger='"+subledger+"',tg.balancesforreporting='"+reporting+"',tg.usedforcalc='"+calculation+
	"',tg.methodwheninpurchaseinv='"+groupdata.allocation+"',tgc.modifiedon=UTC_TIMESTAMP(),tgc.modifiedby='"+user_id+"',tgc.under='"+
	groupdata.under+"' where tg.groupid='"+groupid+"' and tg.companyid='"+companyid+"'";
	execquery.executequery(sql,function(grp_update_data){
		if(grp_update_data.error!=null){
			callback(execquery.buildjson("update_group","Error in Update Qry",grp_update_data.error));
			return;
		}
		callback(execquery.buildjson("update_group","Grp Updated Successfully",null));
	});
};

exports.delete_group = function delete_group(groupid,user_id,companyid,callback){

	var sql="update tbl_group tg join tbl_group_childdetails tgc on tg.groupid=tgc.groupid set tg.deletedon="+
	" UTC_TIMESTAMP(),tg.deletedby='"+user_id+"',tg.deleted=1,tgc.deletedon=UTC_TIMESTAMP(),tgc.deletedby='"+user_id+
	"',tgc.deleted=1 where tg.groupid='"+groupid+"' and tg.companyid='"+companyid+"'";
	execquery.executequery(sql,function(grp_del_data){
		if(grp_del_data.error!=null){
			callback(execquery.buildjson("delete_group","Error in update Qry",grp_del_data.error));
			return;
		}
		callback(execquery.buildjson("delete_group","Deleted Succesfully",null));
	});
};
