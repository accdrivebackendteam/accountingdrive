
exports.executeselectquery = function executeselectquery(query,callback){
	var mysql = require('mysql'); 
	var connection = mysql.createConnection(
    {
	/*
      host     : process.env.OPENSHIFT_MYSQL_DB_HOST,
	  port 	   : process.env.OPENSHIFT_MYSQL_DB_PORT,
	  user     : 'adminUtTpQkn',
	  password : 'x7ZptBmW52bx',
	  database : 'nodejs'
	*/
      host     : 'localhost',
	  port 	   : 3306,
	  user     : 'root',
	  password : 'password',
	  database : 'nodejs'


    });
	
	connection.connect();
	console.log('Query Executed->'+query);
	connection.query(query, function(err, rows, fields) {
		if (err) 
			callback({"data":null,"error":err});
		else
			callback({"data":rows,"error":null});
		});
			console.log('Closing Connection');

	connection.end();
	};



/*Function used for executing insert and update query used*/

exports.executequery = function executequery(query,callback){
	var mysql = require('mysql'); 
	var connection = mysql.createConnection(
    {
/*      host     : process.env.OPENSHIFT_MYSQL_DB_HOST,
	  port 	   : process.env.OPENSHIFT_MYSQL_DB_PORT,
	  user     : 'adminUtTpQkn',
	  password : 'x7ZptBmW52bx',
	  database : 'nodejs' */

      host     : 'localhost',
	  port 	   : 3306,
	  user     : 'root',
	  password : 'password',
	  database : 'nodejs'

    });
	
	connection.connect();
	console.log('Query Executed->'+query);
	connection.query(query, function(err, rows, fields) {
		if (err) 
			callback({"data":null,"error":err});
		else
			callback({"data":rows,"error":null});
		});
	connection.end();
};

/*Function used to build object which is returned to the calling function in routes*/
exports.buildjson = function buildjson(funcname,msg,err)
{
	var status="failure";
	if(err==null)
		status="success";
	var retval={"action":"resp_"+funcname,"response":{"status":status,"msg":msg,"error":err}};
	return retval;
};
