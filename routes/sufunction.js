var mysqlsufunc = require('../model_mysql/sufunc');
var ckuser = require('./chkuser');
var suptfunc = require('./supplementfunc');
var license = require('./crlicense');

/*Checking if user is Super User
 * If so allowing them to create new license
 * Checking is done using connection Id
 * Based on what type of license they are registering for, reating an array 
 * if a document already exists for that specific mail id then appending the array to it
 * else creating a new document and the array is added
 * if its a new document a password is generated and sent to the given mail id
 * */
exports.su_createlicense = function su_createlicense(connid,email,price,user,expireson,company,callback)
	{	
	ckuser.checkusertype(connid,function(user_type){
	console.log('CALLBACK RETURNS '+JSON.stringify(user_type));
	if(user_type.response.error!=null){
		callback(suptfunc.buildjson("su_createlicense","failure","Error in chk user type",user_type.response.error));
		return;
	}
	if(user_type.response.msg.usertype==false){
		callback(suptfunc.buildjson("su_createlicense","failure","Not Super User. Permission Denied",null));
		return;
	}
	var user_id = user_type.response.msg.user_id;
	mysqlsufunc.su_createlicense(connid,user_id,email,price,user,expireson,company,function(create_licesne){
		callback(create_licesne);
	});
	});
};


exports.createuser = function createuser(connid,companyid,email,callback){
	ckuser.getuserid(connid,function(user_id){
	if(user_id.response.error!=null){
		callback(suptfunc.buildjson("createuser","failure","Error in chk user type",user_id.response.error));
		return;
	}
	if(user_id.response.msg.user_id==='undefined'){
		callback(suptfunc.buildjson("createuser","failure","User Not Found",null));
		return;
	}
	var useremail = user_id.response.msg.user_email;
	var user_id = user_id.response.msg.user_id;
	ckuser.checkifadminofcompany(user_id,companyid,function(if_admin){
		if(if_admin.response.error!=null){
			callback(suptfunc.buildjson("createuser","failure","Error in unused user license",if_admin.response.error));
			return;
		}
		if(if_admin.response.msg==false){
			callback(suptfunc.buildjson("createuser","failure","Not Admin. Permission Denied",null));
			return;
		}
		license.unuseduserlicense(connid,function(unused_slot_data){
			if(unused_slot_data.response.error!=null){
				callback(suptfunc.buildjson("createuser","failure","Error in unused user license",unused_slot_data.response.error));
				return;
			}
			if(unused_slot_data.response.msg.count==0){
				callback(suptfunc.buildjson("createuser","failure","No empty slots",null));
				return;
			}
			var slot_id = unused_slot_data.response.msg.empty_slot_id;
			mysqlsufunc.createuser(slot_id,email,user_id,function(new_user_data){
				callback(new_user_data);
			});
		});
	});
});
}

////////////////////////////user license delete test////////////////////////////
exports.su_deleteuserlicense = function su_deleteuserlicense(connid,docid,callback){
    ckuser.checkusertype(connid,'su',resend);
    function resend(data){
        if(data.error!='')
			callback({"action":"resp_su_deleteuserlicense","response":{"status":"failure","msg":"error in usertype","error":data.error}});
        else{
            if(data.msg==null)
				callback({"action":"resp_su_deleteuserlicense","response":{"status":"failure","msg":"not super user","error":""}});
		else{
			user.update({"userlicense._id":docid},{$pull:{"userlicense":{"_id":docid}}},function(err){
				if(err)
					callback({"action":"resp_su_deleteuserlicense","response":{"status":"failure","msg":"Failed","error":""}});
				else
					callback({"action":"resp_su_deleteuserlicense","response":{"status":"success","msg":"Done","error":""}});
				});//update end
			}//usertype not null else end
		}//usertype not err else end
	};//function resend end
};//function end


////////////////////////////////////////////////////delete company license////////////////////////////////////////////

exports.su_deletecompanylicense = function su_deletecompanylicense(connid,docid,callback){
	ckuser.checkusertype(connid,'su',resend);
	function resend(data){
	    if(data.error!='')
			callback({"action":"resp_su_deletecompanylicense","response":{"status":"failure","msg":"error in usertype","error":data.error}});
	    else{
            if(data.msg==null)
				callback({"action":"resp_su_deletecompanylicense","response":{"status":"failure","msg":"not super user","error":""}});
			else{
				user.update({"companylicense._id":docid},{$pull:{"companylicense":{"_id":docid}}},function(err){
					if(err)
						callback({"action":"resp_su_deletecompanylicense","response":{"status":"failure","msg":"Failed","error":""}});
					else
						callback({"action":"resp_su_deletecompanylicense","response":{"status":"success","msg":"Done","error":""}});
					});//update end
				}//usertype not null else end
			}//usertype not error else end
		};//resend func end
	};//function end

////////////////////////////////update/////////////////////////////
exports.su_edituserlicense = function su_edituserlicense(connid,docid,useremail,userid,price,boughton,expireson,callback){
	ckuser.checkusertype(connid,'su',resend);
	function resend(data){
        if(data.error!='')
	        callback({"action":"resp_su_edituserlicense","response":{"status":"failure","msg":"error in usertype","error":data.error}});
		else{
            if(data.msg==null)
				callback({"action":"resp_su_edituserlicense","response":{"status":"failure","msg":"Access Denied. Not super user","error":""}});
			else{
				user.update({"userlicense._id":docid,"userlicense._id":docid},{$set:{"userlicense.$.email":useremail,"userlicense.$.userid":userid,"userlicense.$.price":price,"userlicense.$.boughton":boughton,"userlicense.$.expireson":expireson}},function(err){
					if(err)
						callback({"action":"resp_su_edituserlicense","response":{"status":"failure","msg":"Update Failed","error":err}});
					else
						callback({"action":"resp_su_edituserlicense","response":{"status":"success","msg":"Update Done","error":""}});
					});//update end
				}//_id else end
			}//find one callback else end
			}//function resend end
		};//function end

///////////////////////////////////////company license edit/////////////////////////////////////////////////

exports.su_editcompanylicense = function su_editcompanylicense(connid,docid,companyid,price,boughton,expireson,callback){
    ckuser.checkusertype(connid,'su',resend);
    function resend(data){
	    if(data.error!='')
	        callback({"action":"resp_su_editcompanylicense","response":{"status":"failure","msg":"error in usertype","error":data.error}});
	    else{
	        if(data.msg==null)
				callback({"action":"resp_su_editcompanylicense","response":{"status":"failure","msg":"not super user","error":""}});
			else{
				user.update({"companylicense._id":docid,"companylicense._id":docid},{$set:{"companylicense.$.companyid":companyid,"companylicense.$.price":price,"companylicense.$.boughton":boughton,"companylicense.$.expireson":expireson}},function(err){
					if(err)
						callback({"action":"resp_su_editcompanylicense","response":{"status":"failure","msg":"Update Failed","error":err}});
					else
						callback({"action":"resp_su_editcompanylicense","response":{"status":"success","msg":"Update Done","error":""}});
				});//update end
			}//_id else end
		}//find one callback else end
	};//function resend end
};//function end	
