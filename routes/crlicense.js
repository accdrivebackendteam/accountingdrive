var ckuser = require('./chkuser');
var suptfunc = require('./supplementfunc');
var mysqllicense = require('../model_mysql/license');
/*Creating new License*/


/*Getting user license array for a user
 *Inputs is connid
 * From the conn id retrieve the _id of the document and user type
 * if user type is not su return details of particular user
 * if su return details of all users*/

exports.getuserlicense = function getuserlicense(connid,callback){
	mysqllicense.getuserlicense(connid,function(user_license_data){
		callback(user_license_data);
	});
};

/*Get company license available for a specific user
 *Inputs is connid*/

exports.getcompanylicense = function getcompanylicense(connid,callback){
	mysqllicense.getcompanylicense(connid,function(company_license_data){
		callback(company_license_data);
	});
};

exports.unuseduserlicense = function unuseduserlicense(connid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("unuseduserlicense","failure","Error in get user id",user_id.response.error));
			return;
		}
		mysqllicense.unuseduserlicense(user_id.response.msg.user_email,function(unused_user_license){
			callback(unused_user_license);
		});
	});
};
