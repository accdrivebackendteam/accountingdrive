var ckuser = require('./chkuser');
var suptfunc = require('./supplementfunc');
var rights = require('./rights');
var mysqlgroups = require('../model_mysql/groups');

exports.getall_group = function getall_group(connid,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("getall_group","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'groups','r',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("getall_group","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("getall_group","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysqlgroups.getall_group(companyid,function(all_group_data){
				callback(all_group_data);
			});
		});
	});
};

exports.getdetails_group = function getdetails_group(connid,groupid,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("getdetails_group","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'groups','r',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("getdetails_group","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("getdetails_group","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysqlgroups.getdetails_group(groupid,companyid,function(group_data){
				callback(group_data);
			});
		});
	});
};

exports.create_group = function create_group(connid,groupdata,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("create_group","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'groups','c',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("create_group","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("create_group","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysqlgroups.create_group(user_id,companyid,groupdata,function(new_group_data){
				callback(new_group_data);
			});
		});
	});
};

exports.update_group = function update_group(connid,companyid,groupid,groupdata,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("update_group","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'groups','u',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("update_group","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("update_group","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysqlgroups.update_group(user_id,groupdata,companyid,groupid,function(upd_grp_data){
				callback(upd_grp_data);
			});
		});
	});
};

exports.delete_group = function delete_group(connid,groupid,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("delete_group","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'groups','d',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("delete_group","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("delete_group","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysqlgroups.delete_group(groupid,user_id,companyid,function(del_grp_data){
				callback(del_grp_data);
			});
		});
	});
};
