var chkuser = require('../model_mysql/chkuser');

exports.checkusertype = function checkusertype(connid,callback){
	chkuser.checkusertype(connid,function(user_data){
		callback(user_data);
	});
};

exports.getuserid = function getuserid(connid,callback){
	chkuser.getuserid(connid,function(user_id){
		callback(user_id);
	});
};


exports.checkifadminofcompany = function checkifadminofcompany(userid,companyid,callback){
	chkuser.checkifadminofcompany(userid,companyid,function(if_admin){
		callback(if_admin);
	});
};

exports.settoken = function settoken(oldconnid,newconnid,callback){
	chkuser.settoken(oldconnid,newconnid,function(new_connid){
		callback(new_connid);
	});
};






