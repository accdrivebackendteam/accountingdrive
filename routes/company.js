var ckuser = require('./chkuser');
var userjs = require('./user');
var license = require('./crlicense');
var suptfunc = require('./supplementfunc');
var mysqlcompany = require('../model_mysql/company');
var mysqllicense = require('../model_mysql/license');

exports.userhasunusedcompanylicense = function userhasunusedcompanylicense(connid,callback){//function created to be passed login
	ckuser.getuserid(connid,function(user_id){
	if(user_id.response.error!=null){
		callback(suptfunc("userhasunusedcompanylicense","failure","Error in getting User ID",user_id.response.error));
		return;
	}
	var user_email = user_id.response.msg.user_email;
	mysqllicense.has_empty_companylicense_slot(user_email,function(has_slot){
		if(has_slot.response.error!=null){
			callback(suptfunc.buildjson("userhasunusedcompanylicense","failure","Error in getting Empty Company Slot",has_slot.response.error));
			return;
		}
		callback(suptfunc.buildjson("userhasunusedcompanylicense","success",{"availability":has_slot.response.msg.availability,
		"license_id":has_slot.response.msg.id,"purchase_id":has_slot.response.msg.purchaseid},null));
	});
});
};


exports.getcompanydetails = function getcompanydetails(companyid,connid,callback){
	mysqlcompany.getcompanydetails(companyid,connid,function(companydetails){
		if(companydetails.response.error!=null){
			callback(suptfunc.buildjson("getcompanydetails","failure","Error in getting company details in routes",companydetails.response.error));
			return;
		}
		callback(suptfunc.buildjson("getcompanydetails","success",companydetails.response.msg,null));
		});
};

exports.addcompany = function addcompany(companydetails,connid,callback){
	console.log('company details '+JSON.stringify(companydetails));
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("addcompany","failure","Error in get user id",user_id.response.error));
			return;
		}
		if(user_id.response.msg.user_id==="undefined"){
			callback(suptfunc.buildjson("addcompany","failure","User Not Found",null));
			return;
		}
		var user_email = user_id.response.msg.user_email;
		var user_id = user_id.response.msg.user_id;
		console.log('User Id '+user_id);
		exports.userhasunusedcompanylicense(connid,function(has_slot){
			if(has_slot.response.error!=null){
				callback(suptfunc.buildjson("addcompany","failure","Error in cmp has slot qry",has_slot.response.error));
				return;
			}
			if(has_slot.response.msg.availability==false){
				callback(suptfunc.buildjson("addcompany","failure","No Empty Slot",null));
				return;
			}
			if(has_slot.response.msg.availability==true){
				console.log('Has Slot'+JSON.stringify(has_slot));
				var cmp_license_rec_id = has_slot.response.msg.license_id;
				var purchase_id = has_slot.response.msg.purchase_id;
				mysqlcompany.createcompany(companydetails,cmp_license_rec_id,user_id,purchase_id,user_email,function(company_data){
				if(company_data.response.error!=null){
					callback(suptfunc.buildjson("addcompany","failure","Error in createcompany",company_data.response.error));
					return;
				}
				callback(suptfunc.buildjson("addcompany","success","Company Created Successfully",null));
				});
			}
		});
		
	});
};//create company function end

exports.updatecompanydetails = function updatecompanydetails(connid,companyid,companydetails,callback){
	ckuser.getuserid(connid,function(user_id){//getting user id for the given connection id
	if(user_id.response.error!=null)
		callback(suptfunc.buildjson("updatecompanydetails","failure","error in get user id",user_id.response.error));
	else if(user_id.response.msg.user_id==='undefined')
		callback(suptfunc.buildjson("updatecompanydetails","failure","User Not Found",null));
	else{
		ckuser.checkifadminofcompany(user_id.response.msg.user_id,companyid,function(if_admin){
		if(if_admin.response.error!=null)
			callback(suptfunc.buildjson("updatecompanydetails","failure","error in check if admin",if_admin.response.error));
		else if(if_admin.response.msg==false)
			callback(suptfunc.buildjson("updatecompanydetails","failure","User is Not Admin Of Company",null));
		else{
			mysqlcompany.updatecompany(companyid,companydetails,user_id.response.msg.user_id,function(cmp_upd_data){
			if(cmp_upd_data.response.error!=null){
				callback(suptfunc.buildjson("updatecompanydetails","failure","Error in update qry",cmp_upd_data.response.error));
				return;
			}
			callback(suptfunc.buildjson("updatecompanydetails","success",cmp_upd_data.response.msg,null));
			});
			}
		});
		}
	});
};


exports.addusertocompany = function addusertocompany(connid,companyid,emailid,callback){
	ckuser.getuserid(connid,function(user_id){
	if(user_id.response.error!=null){
		callback(suptfunc.buildjson("addusertocompany","failure","error in get user id",user_id.response.error));
		return;
	}
	if(user_id.response.msg.user_id==='undefined'){
		callback(suptfunc.buildjson("addusertocompany","failure","User Not Found",null));
		return;
	}
	var user_id = user_id.response.msg.user_id;
	license.unuseduserlicense(connid,function(has_userlicense_slot){
		if(has_userlicense_slot.response.error!=null){
			callback(suptfunc.buildjson("addusertocompany","failure","error in unused user license",has_userlicense_slot.response.error));
			return;
		}
		if(has_userlicense_slot.response.msg[0].cnt==0){
			callback(suptfunc.buildjson("addusertocompany","failure","No Empty Slot",null));
			return;
		}
		var license_id = has_userlicense_slot.response.msg[0].licenseid;
		ckuser.checkifadminofcompany(user_id,companyid,function(is_user_admin){
			if(is_user_admin.response.error!=null){
				callback(suptfunc.buildjson("addusertocompany","failure","error in check if admin query",is_user_admin.response.error));
				return;
			}
			if(is_user_admin.response.msg==false){
				callback(suptfunc.buildjson("addusertocompany","failure","Not Admin of Company",null));
				return;
			}
			mysqlcompany.addusertocompany(user_id,emailid,companyid,licene_id,function(new_user_data){
				callback(new_user_data);
			});
		});
	});
	});
};

exports.getdetailsofassociatedcompany = function getdetailsofassociatedcompany(companyid,i,callback){
	company.findOne({"_id":companyid},"_id name",function(err,cmp_data){
	if(err)
		callback({"action":"resp_getdetailsofassociatedcompany","response":{"status":"failure","msg":"error in company find one query","error":err}});
	else if(cmp_data==null)
		callback({"action":"resp_getdetailsofassociatedcompany","response":{"status":"failure","msg":{},"error":""}});
	else if(cmp_data!=null)
		callback({"action":"resp_getdetailsofassociatedcompany","response":{"status":"success","msg":{"companyname":cmp_data.name,"companyid":cmp_data._id},"iter":i,"error":""}});
	});
};

exports.removeuserfromcompany = function removeuserfromcompany(connid,rm_user_email,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
	if(user_id.response.error!='')
		callback({"action":"resp_removeuserfromcompany","response":{"status":"failure","msg":"error in getting user id","error":user_id.response.error}});
	else if(user_id.response.msg.id==null)
			callback({"action":"resp_removeuserfromcompany","response":{"status":"failure","msg":"User Not Found","error":""}});
	else if(user_id.response.msg.id!=null){
		user.update({$and:[{"_id":user_id.response.msg.id},{"userlicense.email":rm_user_email}]},{$set:{"userlicense.$.email":""}},
		function(err){
		if(err)
			callback({"action":"resp_removeuserfromcompany","response":{"status":"failure","msg":"error in updating user license","error":err}});
		else{
			user.update({"email":rm_user_email},{$pull:{"userrights":{"companyid":companyid}}},function(err){
			if(err)
				callback({"action":"resp_removeuserfromcompany","response":{"status":"failure","msg":"error in updating user rights array","error":err}});
			else	
				callback({"action":"resp_removeuserfromcompany","response":{"status":"success","msg":"User Removed","error":""}});
			});
			}
		});
		}
	});
};

exports.getusermappeddetails = function getusermappeddetails(connid,companyid,callback){
	mysqlcompany.getusermappeddetails(companyid,function(user_mapped){
		callback(user_mapped);
	});
};

