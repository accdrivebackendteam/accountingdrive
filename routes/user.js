var sqluser = require('../model_mysql/user');

/* 
 * Login Function
 * Details obtained is passed to another js file based upon db used
 * Output is returned from that file na dsent back to user
 * Response name: resp_login
 * Output Returned->loggedin:bool,connid,usertype:string(normal or su),userhascompanylicense:int,
 * associatedcompanylist:array[companyid,name],user_id
 * */

exports.login = function login(email,password,ipaddress,connid,callback){
	sqluser.checklogin(email,password,ipaddress,connid,function(login_data){
		callback(login_data);
	});
};

exports.getassociatedcompanylist = function getassociatedcompanylist(connid,callback){
	sqluser.getassociatedcompanylist(connid,function(company_data){
		callback(company_data);
	});
};
/*
 * Logout Function
 * user id is obtained from front end
 * Response name: resp_logout
 * If query fails error is passed else success msg is passed
 * */
exports.logout = function logout(connid,user_id,callback){
	sqluser.logout(connid,user_id,function(logout_data){
		callback(logout_data);
	});
};

/*
 * Force Logout
 * Email is obtained from front end
 * Response name: resp_forcelogout
 * Response is same as logout function
 * */
exports.forcelogout = function forcelogout(email,callback){
	sqluser.forcelogout(email,function(force_logout_data){
		callback(force_logout_data);
	});
};
