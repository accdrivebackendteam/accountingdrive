var ckuser = require('./chkuser');
var suptfunc = require('./supplementfunc');
var rights = require('./rights');
var mysql_unitsofmeasure = require('../model_mysql/unitsofmeasure');

exports.getall_simple_unitsofmeasure = function getall_simple_unitsofmeasure(connid,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("getall_simple_unitsofmeasure","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'unitsofmeasure','r',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("getall_simple_unitsofmeasure","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("getall_simple_unitsofmeasure","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysql_unitsofmeasure.getall_simple_unitsofmeasure(companyid,function(all_units_data){
				callback(all_units_data);
			});
		});
	});
};

exports.getdetails_simple_unitsofmeasure = function getdetails_simple_unitsofmeasure(connid,unit_id,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("getdetails_simple_unitsofmeasure","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'unitsofmeasure','r',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("getdetails_simple_unitsofmeasure","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("getdetails_simple_unitsofmeasure","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysql_unitsofmeasure.getdetails_simple_unitsofmeasure(unit_id,companyid,function(units_data){
				callback(units_data);
			});
		});
	});
};

exports.create_simple_unitsofmeasure = function create_simple_unitsofmeasure(connid,companyid,unitsofmeasure,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("create_simple_unitsofmeasure","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'unitsofmeasure','c',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("create_simple_unitsofmeasure","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("create_simple_unitsofmeasure","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysql_unitsofmeasure.create_simple_unitsofmeasure(companyid,user_id,unitsofmeasure,function(units_data){
				callback(units_data);
			});
		});
	});
};

exports.update_simple_unitsofmeasure = function update_simple_unitsofmeasure(connid,unit_id,unitsofmeasure,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("update_simple_unitsofmeasure","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'unitsofmeasure','u',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("update_simple_unitsofmeasure","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("update_simple_unitsofmeasure","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysql_unitsofmeasure.update_simple_unitsofmeasure(unit_id,user_id,unitsofmeasure,companyid,function(upd_units_data){
				callback(upd_units_data);
			});
		});
	});
};

exports.delete_simple_unitsofmeasure = function delete_simple_unitsofmeasure(connid,unit_id,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("delete_simple_unitsofmeasure","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'unitsofmeasure','d',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("delete_simple_unitsofmeasure","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("delete_simple_unitsofmeasure","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysql_unitsofmeasure.delete_simple_unitsofmeasure(unit_id,user_id,companyid,function(units_data){
				callback(units_data);
			});
		});
	});
};

exports.getall_compound_unitsofmeasure = function getall_compound_unitsofmeasure(connid,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("getall_compound_unitsofmeasure","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'unitsofmeasure','r',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("getall_compound_unitsofmeasure","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("getall_compound_unitsofmeasure","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysql_unitsofmeasure.getall_compound_unitsofmeasure(companyid,function(all_units_data){
				callback(all_units_data);
			});
		});
	});
};

exports.getdetails_compound_unitsofmeasure = function getdetails_compound_unitsofmeasure(connid,unit_id,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("getdetails_compound_unitsofmeasure","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'unitsofmeasure','r',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("getdetails_compound_unitsofmeasure","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("getdetails_compound_unitsofmeasure","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysql_unitsofmeasure.getdetails_compound_unitsofmeasure(unit_id,companyid,function(units_data){
				callback(units_data);
			});
		});
	});
};

exports.create_compound_unitsofmeasure = function create_compound_unitsofmeasure(connid,companyid,unitsofmeasure,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("create_compound_unitsofmeasure","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'unitsofmeasure','c',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("create_compound_unitsofmeasure","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("create_compound_unitsofmeasure","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysql_unitsofmeasure.create_compound_unitsofmeasure(companyid,user_id,unitsofmeasure,function(units_data){
				callback(units_data);
			});
		});
	});
};

exports.update_compound_unitsofmeasure = function update_compound_unitsofmeasure(connid,unit_id,unitsofmeasure,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("update_compound_unitsofmeasure","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'unitsofmeasure','u',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("update_compound_unitsofmeasure","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("update_compound_unitsofmeasure","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysql_unitsofmeasure.update_compound_unitsofmeasure(unit_id,user_id,unitsofmeasure,companyid,function(upd_units_data){
				callback(upd_units_data);
			});
		});
	});
};

exports.delete_compound_unitsofmeasure = function delete_compound_unitsofmeasure(connid,unit_id,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("delete_compound_unitsofmeasure","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'unitsofmeasure','d',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("delete_compound_unitsofmeasure","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("delete_compound_unitsofmeasure","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysql_unitsofmeasure.delete_compound_unitsofmeasure(unit_id,user_id,companyid,function(units_data){
				callback(units_data);
			});
		});
	});
};
