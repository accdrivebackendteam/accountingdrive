var ckuser = require('./chkuser');
var userjs = require('./user');
var license = require('./crlicense');
var suptfunc = require('./supplementfunc');
var mysqlcompany = require('../model_mysql/company');
var mysqllicense = require('../model_mysql/license');

exports.userhasunusedcompanylicense = function userhasunusedcompanylicense(connid,callback){//function created to be passed login
	ckuser.getuserid(connid,function(user_id){
	if(user_id.response.error!=null){
		callback(suptfunc("userhasunusedcompanylicense","failure","Error in getting User ID",user_id.response.error));
		return;
	}
	var user_email = user_id.response.msg.user_email;
	mysqllicense.has_empty_companylicense_slot(user_email,function(has_slot){
		if(has_slot.response.error!=null){
			callback(suptfunc.buildjson("userhasunusedcompanylicense","failure","Error in getting Empty Company Slot",has_slot.response.error));
			return;
		}
		callback(suptfunc.buildjson("userhasunusedcompanylicense","success",{"availability":has_slot.response.msg.availability,
		"license_id":has_slot.response.msg.id,"purchase_id":has_slot.response.msg.purchaseid},null));
	});
});
};


exports.getcompanydetails = function getcompanydetails(connid,callback){
	ckuser.getcompanydetails(connid,function(companydetails){
		if(companydetails.response.error!=null){
			callback(suptfunc.buildjson("getcompanydetails","failure","Error in getting company details in routes",companydetails.response.error));
			return;
		}
		callback(suptfunc.buildjson("getcompanydetails","success",{"companydetails":companydetails.response.msg},null));
		});
};

exports.associatedcompanylist = function associatedcompanylist(userid,callback){
	var companylist=[];
	user.findOne({"_id":userid},{'_id':0,"userrights":1},function(err,data){
	if(err)
		callback({"action":"resp_associatedcompanylist","response":{"status":"failure","msg":"error in associated company list query","error":err}});
	else{
		if(data.userrights.length==0)
			callback({"action":"resp_associatedcompanylist","response":{"status":"failure","msg":"Not associated with any company","error":""}});
		else{
			for(var i=0; i<data.userrights.length; i++)
				{
				exports.getdetailsofassociatedcompany(data.userrights[i].companyid,i,function(assoc_cmp_details){
				if(assoc_cmp_details.response.error!='')
					callback({"action":"resp_associatedcompanylist","response":{"status":"failure","msg":"error in associated company list query","error":assoc_cmp_details.response.error}});
				else{
					companylist.push(assoc_cmp_details.response.msg);
					if(assoc_cmp_details.response.iter+1==data.userrights.length)
						callback({"action":"resp_associatedcompanylist","response":{"status":"success","msg":companylist,"error":""}});
					}
				});
			}
		}
	}
	});
};


exports.addcompany = function addcompany(companydetails,connid,callback){
	console.log('company details '+JSON.stringify(companydetails));
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("addcompany","failure","Error in get user id",user_id.response.error));
			return;
		}
		if(user_id.response.msg.user_id==="undefined"){
			callback(suptfunc.buildjson("addcompany","failure","User Not Found",null));
			return;
		}
		var user_email = user_id.response.msg.user_email;
		var user_id = user_id.response.msg.user_id;
		console.log('User Id '+user_id);
		exports.userhasunusedcompanylicense(connid,function(has_slot){
			if(has_slot.response.error!=null){
				callback(suptfunc.buildjson("addcompany","failure","Error in cmp has slot qry",has_slot.response.error));
				return;
			}
			if(has_slot.response.msg.availability==false){
				callback(suptfunc.buildjson("addcompany","failure","No Empty Slot",null));
				return;
			}
			if(has_slot.response.msg.availability==true){
				console.log('Has Slot'+JSON.stringify(has_slot));
				var cmp_license_rec_id = has_slot.response.msg.license_id;
				var purchase_id = has_slot.response.msg.purchase_id;
				mysqlcompany.createcompany(companydetails,cmp_license_rec_id,user_id,purchase_id,user_email,function(company_data){
				if(company_data.response.error!=null){
					callback(suptfunc.buildjson("addcompany","failure","Error in createcompany",company_data.response.error));
					return;
				}
				callback(suptfunc.buildjson("addcompany","success","Company Created Successfully",null));
				});
			}
		});
		
	});
};//create company function end



/*exports.updatecompanydetails = function updatecompanydetails(connid,companyid,callback){
	ckuser.getuserid(connid,function(user_id){//getting user id for the given connection id
	if(user_id.response.error!='')
		callback({"action":"resp_updatecompanydetails","response":{"status":"failure","msg":"error in get user id","error":user_id.response.error}});
	else if(user_id.response.msg.id==null)
		callback({"action":"resp_updatecompanydetails","response":{"status":"failure","msg":"User Not Found","error":""}});
	else{
		ckuser.checkifadminofcompany(user_id.response.msg.id,companyid,function(if_admin){
		if(if_admin.response.error!='')
			callback({"action":"resp_updatecompanydetails","response":{"status":"failure","msg":"error in check if admin","error":if_admin.response.error}});
		else if(if_admin.response.msg==false)
			callback({"action":"resp_updatecompanydetails","response":{"status":"failure","msg":"User is Not Admin Of Company","error":""}});
		else{
			company.update({"_id":companyid},{$set:{}},function(err){
			if(err)
				callback({"action":"resp_updatecompanydetails","response":{"status":"failure","msg":"error in company findOne","error":err}});
			else if(company_data)
				callback({"action":"resp_updatecompanydetails","response":{"status":"failure","msg":"No Company Found for Id","error":""}});
			else
				callback({"action":"resp_updatecompanydetails","response":{"status":"failure","msg":company_data,"error":""}});
			});
			}
		});
		}
	});
};
*/


exports.addusertocompany = function addusertocompany(connid,companyid,emailid,callback){
	ckuser.getuserid(connid,function(user_id){//getting user id for the given connection id
	if(user_id.response.error!='')
		callback({"action":"resp_addusertocompany","response":{"status":"failure","msg":"error in get user id","error":user_id.response.error}});
	else if(user_id.response.msg.id==null)
		callback({"action":"resp_addusertocompany","response":{"status":"failure","msg":"User Not Found","error":""}});
	else{
		license.has_empty_userlicense_slot(user_id.response.msg.id,function(has_userlicense_slot){//checking if the user has empty slots in user license array
		if(has_userlicense_slot.response.error!='')
			callback({"action":"resp_addusertocompany","response":{"status":"failure","msg":"error in has_empty_userlicense_slot id","error":has_userlicense_slot.response.error}});
		else if(has_userlicense_slot.response.availability==false)
			callback({"action":"resp_addusertocompany","response":{"status":"failure","msg":"No Empty Slot","error":""}});
		else if(has_userlicense_slot.response.availability==true){//if there is empty slot
			ckuser.checkifadminofcompany(user_id.response.msg.id,companyid,function(is_user_admin){//checking if the user is admin for the specific company
			if(err)
				callback({"action":"resp_addusertocompany","response":{"status":"failure","msg":"error in check if admin query","error":is_user_admin.error}});
			else if(is_user_admin.response.msg==false)
				callback({"action":"resp_addusertocompany","response":{"status":"failure","msg":"Not Admin of Company","error":""}});
			else if(is_user_admin.response.msg==true){//if the user is admin updating the user license to contain the email passed
				user.update({$and:[{"_id":user_id.response.msg.id},{"userlicense._id":has_userlicense_slot.response.data.userlicense[0]._id}]},
				{$set:{"userlicense.$.email":emailid}},function(err){
				if(err)
					callback({"action":"resp_addusertocompany","response":{"status":"failure","msg":"error in update query","error":""}});
				else{
					user.findOne({"email":emailid},{},function(err,new_user_data){//checking id doc exists for the email passed
					if(err)
						callback({"action":"resp_addusertocompany","response":{"status":"failure","msg":"error in findOne","error":err}});
					else if(new_user_data==null){
						passwd.passwordgenerator(email,function(user_passwd){
						if(user_passwd.response.error!='')
							callback({"action":"resp_addusertocompany","response":{"status":"failure","msg":"error in hashing","error":user_passwd.response.error}});
						else{
							var userinst = new user({name:emailid,email:emailid,password:user_passwd.response.msg,
								"userrights":{companyid:companyid,isadmin:false,companyuserrights:[
									{modulename:"groups",rights:"xrxx"},
									{modulename:"ledgers",rights:"xrxx"},
									{modulename:"vouchertypes",rights:"xrxx"},
									{modulename:"inventoryinfo",rights:"xrxx"},
									{modulename:"stockgroups",rights:"xrxx"},
									{modulename:"stockcategories",rights:"xrxx"},
									{modulename:"stockitems",rights:"xrxx"},
									{modulename:"unitsofmeasure",rights:"xrxx"},
									{modulename:"excise",rights:"xrxx"},
									{modulename:"excisefordealer",rights:"xrxx"},
									{modulename:"valueaddedtax",rights:"xrxx"},
									{modulename:"taxdeductedatsource",rights:"xrxx"},
									{modulename:"taxcollectedatsource",rights:"xrxx"},
									{modulename:"servicetax",rights:"xrxx"},
									{modulename:"accountingvouchers",rights:"xrxx"},
									{modulename:"inventoryvouchers",rights:"xrxx"},
									{modulename:"ordervouchers",rights:"xrxx"},
									{modulename:"importofdata",rights:"xrxx"},
									{modulename:"banking",rights:"xrxx"},
									{modulename:"trialbalance",rights:"xrxx"},
									{modulename:"daybook",rights:"xrxx"},
									{modulename:"accountsbook",rights:"xrxx"},
									{modulename:"statementsofaccounts",rights:"xrxx"},
									{modulename:"inventorybooks",rights:"xrxx"},
									{modulename:"statementofinventory",rights:"xrxx"},
									{modulename:"cash/fundsflow",rights:"xrxx"},
									{modulename:"receiptsandpayments",rights:"xrxx"},
									{modulename:"listofaccounts",rights:"xrxx"},
									{modulename:"exceptionreports",rights:"xrxx"},
									{modulename:"balancesheet",rights:"xrxx"},
									{modulename:"profit&lossA/c",rights:"xrxx"},
									{modulename:"stocksummary",rights:"xrxx"},
									{modulename:"ratioanalysis",rights:"xrxx"}]
								}
							});
							userinst.save(function(err,user_inst_data){
							if(err)
								callback({"action":"resp_addusertocompany","response":{"status":"failure","msg":"error in creating user doc","error":err}});
							else
								callback({"action":"resp_addusertocompany","response":{"status":"success","msg":"User Added Succesfully","error":""}});
							});
						}
						});
					}
					else if(new_user_data!=null){
						user.update({"email":emailid},{$push:{"userrights":{companyid:companyid,isadmin:false,
							companyuserrights:[
									{modulename:"groups",rights:"xrxx"},
									{modulename:"ledgers",rights:"xrxx"},
									{modulename:"vouchertypes",rights:"xrxx"},
									{modulename:"inventoryinfo",rights:"xrxx"},
									{modulename:"stockgroups",rights:"xrxx"},
									{modulename:"stockcategories",rights:"xrxx"},
									{modulename:"stockitems",rights:"xrxx"},
									{modulename:"unitsofmeasure",rights:"xrxx"},
									{modulename:"excise",rights:"xrxx"},
									{modulename:"excisefordealer",rights:"xrxx"},
									{modulename:"valueaddedtax",rights:"xrxx"},
									{modulename:"taxdeductedatsource",rights:"xrxx"},
									{modulename:"taxcollectedatsource",rights:"xrxx"},
									{modulename:"servicetax",rights:"xrxx"},
									{modulename:"accountingvouchers",rights:"xrxx"},
									{modulename:"inventoryvouchers",rights:"xrxx"},
									{modulename:"ordervouchers",rights:"xrxx"},
									{modulename:"importofdata",rights:"xrxx"},
									{modulename:"banking",rights:"xrxx"},
									{modulename:"trialbalance",rights:"xrxx"},
									{modulename:"daybook",rights:"xrxx"},
									{modulename:"accountsbook",rights:"xrxx"},
									{modulename:"statementsofaccounts",rights:"xrxx"},
									{modulename:"inventorybooks",rights:"xrxx"},
									{modulename:"statementofinventory",rights:"xrxx"},
									{modulename:"cash/fundsflow",rights:"xrxx"},
									{modulename:"receiptsandpayments",rights:"xrxx"},
									{modulename:"listofaccounts",rights:"xrxx"},
									{modulename:"exceptionreports",rights:"xrxx"},
									{modulename:"balancesheet",rights:"xrxx"},
									{modulename:"profit&lossA/c",rights:"xrxx"},
									{modulename:"stocksummary",rights:"xrxx"},
									{modulename:"ratioanalysis",rights:"xrxx"}]
								}}},function(err){							
								if(err)
									callback({"action":"resp_addusertocompany","response":{"status":"failure","msg":"error in Updating user rights to new user","error":err}});
								else
									callback({"action":"resp_addusertocompany","response":{"status":"success","msg":"User Added Succesfully","error":""}});
								});
								}
							});
							}
						});
						}
					});
					}
				});
			}
		});
	};

exports.getdetailsofassociatedcompany = function getdetailsofassociatedcompany(companyid,i,callback){
	company.findOne({"_id":companyid},"_id name",function(err,cmp_data){
	if(err)
		callback({"action":"resp_getdetailsofassociatedcompany","response":{"status":"failure","msg":"error in company find one query","error":err}});
	else if(cmp_data==null)
		callback({"action":"resp_getdetailsofassociatedcompany","response":{"status":"failure","msg":{},"error":""}});
	else if(cmp_data!=null)
		callback({"action":"resp_getdetailsofassociatedcompany","response":{"status":"success","msg":{"companyname":cmp_data.name,"companyid":cmp_data._id},"iter":i,"error":""}});
	});
};

exports.removeuserfromcompany = function removeuserfromcompany(connid,rm_user_email,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
	if(user_id.response.error!='')
		callback({"action":"resp_removeuserfromcompany","response":{"status":"failure","msg":"error in getting user id","error":user_id.response.error}});
	else if(user_id.response.msg.id==null)
			callback({"action":"resp_removeuserfromcompany","response":{"status":"failure","msg":"User Not Found","error":""}});
	else if(user_id.response.msg.id!=null){
		user.update({$and:[{"_id":user_id.response.msg.id},{"userlicense.email":rm_user_email}]},{$set:{"userlicense.$.email":""}},
		function(err){
		if(err)
			callback({"action":"resp_removeuserfromcompany","response":{"status":"failure","msg":"error in updating user license","error":err}});
		else{
			user.update({"email":rm_user_email},{$pull:{"userrights":{"companyid":companyid}}},function(err){
			if(err)
				callback({"action":"resp_removeuserfromcompany","response":{"status":"failure","msg":"error in updating user rights array","error":err}});
			else	
				callback({"action":"resp_removeuserfromcompany","response":{"status":"success","msg":"User Removed","error":""}});
			});
			}
		});
		}
	});
};


exports.hasunusedcompanylicense = function hasunusedcompanylicense(connid,callback){
	ckuser.getuserid(connid,function(user_id){
	if(user_id.response.error!='')
		callback({"action":"resp_hasunusedcompanylicense","response":{"status":"failure","msg":"error in getting user id","error":user_id.response.error}});
	else if(user_id.response.msg.id==null)
			callback({"action":"resp_hasunusedcompanylicense","response":{"status":"failure","msg":"User Not Found","error":""}});
	else if(user_id.response.msg.id!=null){
		license.has_empty_companylicense_slot(user_id.response.msg.id,connid,function(has_empty_cmp_slot){
		if(has_empty_cmp_slot.response.error!='')
			callback({"action":"resp_hasunusedcompanylicense","response":{"status":"failure","msg":"error in query","error":has_empty_cmp_slot.response.error}});
		else
			callback({"action":"resp_hasunusedcompanylicense","response":{"status":"success","msg":has_empty_cmp_slot.response.msg,"error":""}});
		});
		}
	});
};


exports.getuserlicensedetails = function getuserlicensedetails(connid,callback){
	var unuseduserlicense=[];
	var useduserlicense=[];
	ckuser.getuserid(connid,function(user_id){
	if(user_id.response.error!='')
		callback({"action":"resp_getuserlicensedetails","response":{"status":"failure","msg":"error in getting user id","error":user_id.response.error}});
	else if(user_id.msg.id==null)
			callback({"action":"resp_getuserlicensedetails","response":{"status":"failure","msg":"User Not Found","error":""}});
	else if(user_id.response.msg.id!=null){
		user.findOne({"_id":user_id.response.msg.id},{"userlicense":1},function(err,user_license){
		if(err)
			callback({"action":"resp_getuserlicensedetails","response":{"status":"failure","msg":"error in getting user license","error":err}});
		else{
			for(var i=1; i<user_license.userlicense.length;i++){
				if(user_license.userlicense[i].email==''){
					unuseduserlicense.push(user_license.userlicense[i]._id);
				}
				else{
					user.findOne({"_id":user_license.userlicense[i].userid},{"name":1},function(err,user_data){
					if(err)
						callback({"action":"resp_getuserlicensedetails","response":{"status":"failure","msg":"error in getting user id","error":err}});
					else
						useduserlicense.push(user_data.name);
					});
				}
			if(user_license.userlicense[i]+1==user_license.userlicense[i].length)
				callback({"action":"resp_getuserlicensedetails","response":{"status":"success","msg":{"unusedlicense":unuseduserlicense,"usedlicense":useduserlicense},"error":""}});
			}
		}	
		});
	}
	});
}
