var ckuser = require('./chkuser');
var suptfunc = require('./supplementfunc');
var rights = require('./rights');
var mysql_stockgroups = require('../model_mysql/stockgroups');

exports.getall_stockgroups = function getall_stockgroups(connid,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("getall_stockgroups","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'stockgroups','r',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("getall_stockgroups","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("getall_stockgroups","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysql_stockgroups.getall_stockgroups(companyid,function(all_stockgroup_data){
				callback(all_stockgroup_data);
			});
		});
	});
};

exports.getdetails_stockgroups = function getdetails_stockgroups(connid,stk_grpid,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("getdetails_stockgroups","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'stockgroups','r',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("getdetails_stockgroups","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("getdetails_stockgroups","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysql_stockgroups.getdetails_stockgroups(stk_grpid,companyid,function(stockgroup_data){
				callback(stockgroup_data);
			});
		});
	});
};


exports.create_stockgroups = function create_stockgroups(connid,stockgroupdata,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("create_stockgroups","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'stockgroups','c',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("create_stockgroups","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("create_stockgroups","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysql_stockgroups.create_stockgroups(user_id,companyid,stockgroupdata,function(new_stockgroup_data){
				callback(new_stockgroup_data);
			});
		});
	});
};

exports.update_stockgroups = function update_stockgroups(connid,stk_grpid,stockgroupdata,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("update_stockgroups","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'groups','u',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("update_stockgroups","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("update_stockgroups","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysql_stockgroups.update_stockgroups(user_id,companyid,stk_grpid,stockgroupdata,function(upd_stk_grp_data){
				callback(upd_stk_grp_data);
			});
		});
	});
};

exports.delete_stockgroups = function delete_stockgroups(connid,stk_groupid,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("delete_stockgroups","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'groups','d',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("delete_stockgroups","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("delete_stockgroups","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysql_stockgroups.delete_stockgroups(stk_groupid,user_id,companyid,function(del_stk_grp_data){
				callback(del_stk_grp_data);
			});
		});
	});
};

