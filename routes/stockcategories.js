var ckuser = require('./chkuser');
var suptfunc = require('./supplementfunc');
var rights = require('./rights');
var mysql_stockcategories = require('../model_mysql/stockcategories');

exports.getall_stockcategories = function getall_stockcategories(connid,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("getall_stockcategories","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'stockcategories','r',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("getall_stockcategories","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("getall_stockcategories","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysql_stockcategories.getall_stockcategories(companyid,function(all_stockcatg_data){
				callback(all_stockcatg_data);
			});
		});
	});
};

exports.getdetails_stockcategories = function getdetails_stockcategories(connid,stk_catgid,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("getdetails_stockcategories","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'stockcategories','r',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("getdetails_stockcategories","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("getdetails_stockcategories","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysql_stockcategories.getdetails_stockcategories(stk_catgid,companyid,function(stockcategories_data){
				callback(stockcategories_data);
			});
		});
	});
};


exports.create_stockcategories = function create_stockcategories(connid,stockcatgdata,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("create_stockcategories","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'stockcategories','c',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("create_stockcategories","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("create_stockcategories","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysql_stockcategories.create_stockcategories(user_id,companyid,stockcatgdata,function(new_stockcatg_data){
				callback(new_stockcatg_data);
			});
		});
	});
};

exports.update_stockcategories = function update_stockcategories(connid,stk_catgid,stockcatgdata,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("update_stockcategories","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'groups','u',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("update_stockcategories","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("update_stockcategories","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysql_stockcategories.update_stockcategories(user_id,stk_catgid,companyid,stockcatgdata,function(upd_stk_catg_data){
				callback(upd_stk_catg_data);
			});
		});
	});
};

exports.delete_stockcategories = function delete_stockcategories(connid,stk_catgid,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("delete_stockcategories","failure","Error in getuserid",user_id.response.error));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'groups','d',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("delete_stockcategories","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("delete_stockcategories","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysql_stockcategories.delete_stockcategories(stk_catgid,user_id,companyid,function(del_stk_catg_data){
				callback(del_stk_catg_data);
			});
		});
	});
};

