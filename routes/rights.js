var ckuser = require('./chkuser');
var userjs = require('./user');
var license = require('./crlicense');
var suptfunc = require('./supplementfunc');
var mysqlrights = require('../model_mysql/rights');

/*Getting rights for individual user to a specific company.
 *Inputs are userid and company id*/

exports.getuserrightsforcompany = function getuserrightsforcompany(connid,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
	if(user_id.response.error!=null)
		callback(suptfunc.buildjson("getuserrightsforcompany","failure","Could not get User ID",user_id.response.error));
	else if(user_id.response.msg.user_id==='undefined')
		callback(suptfunc.buildjson("getuserrightsforcompany","failure","User Not Found",null));
	else{
		mysqlrights.getuserrightsforcompany(companyid,user_id.response.msg.user_id,function(rights_data){
			if(rights_data.response.error!=null){
				callback(rights_data);
				return;
			}
			callback(rights_data);
		});
	}
});
};

/*Setting rights for the user to a specific company for each group
 *Inputs are connid,companyid,userrights*/

exports.setuserrightsforcompany = function setuserrightsforcompany(connid,companyid,upd_userid,userrights,callback){
	ckuser.getuserid(connid,function(user_id){
	if(user_id.response.error!=null)
		callback(suptfunc.buildjson("setuserrightsforcompany","failure","error in userid query",user_id.response.error));
	else if(user_id.response.msg.user_id==='undefined')
		callback(suptfunc.buildjson("setuserrightsforcompany","failure","User Not Found",null));
	else{
		ckuser.checkifadminofcompany(user_id.response.msg.user_id,companyid,function(is_user_admin){
		if(is_user_admin.response.error!=null)
			callback(suptfunc.buildjson("setuserrightsforcompany","failure","error in is admin query",is_user_admin.response.error));
		else if(is_user_admin.response.msg==false)
			callback(suptfunc.buildjson("setuserrightsforcompany","failure","User Not Admin for Company",null));
		else{
				mysqlrights.setuserrightsforcompany(userrights,user_id.response.msg.user_id,companyid,upd_userid,function(perm_data){
					callback(perm_data);
				});
			
			}
			});
		}
	});
};

exports.checkuserrights = function checkuserrights(user_id,companyid,rightsfor,action,callback){
	mysqlrights.checkuserrights(user_id,companyid,rightsfor,action,function(has_rights){
		callback(has_rights);
	});
};
