var ckuser = require('./chkuser');
var suptfunc = require('./supplementfunc');
var mysqlledger = require('../model_mysql/ledger');
var rights = require('./rights');

exports.createledger = function createledger(connid,ledgerdata,companyid,callback){
	ckuser.getuserid(connid,function(user_id){
		if(user_id.response.error!=null){
			callback(suptfunc.buildjson("createledger","failure","Error in get User id",user_id.response.error));
			return;
		}
		if(user_id.response.msg.user_id==='undefined'){
			callback(suptfunc.buildjson("createledger","failure","User Not Found",null));
			return;
		}
		var user_id = user_id.response.msg.user_id;
		rights.checkuserrights(user_id,companyid,'ledgers','c',function(has_rights){
			if(has_rights.response.error!=null){
				callback(suptfunc.buildjson("createledger","failure","Error in checkuserright",has_rights.response.error));
				return;
			}
			if(has_rights.response.msg==false){
				callback(suptfunc.buildjson("createledger","failure","You do not have rights to perform the operation",null));
				return;
			}
			mysqlledger.createledger(user_id,ledgerdata,companyid,function(ledger_data){
				callback(ledger_data);
			});
		});
	});
};
