var app = require('express')()
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server);

app.configure(function(){
	app.set('port',process.env.OPENSHIFT_INTERNAL_PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080);
	app.set('ipaddr', process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1");
	});


// routing
app.get('/html/*', function (req, res) {
path = req.params[0] ? req.params[0] : 'index.html';
  res.sendfile(__dirname + '/html/'+path);  
});

/*
app.get('/index', function (req, res) {
  res.sendfile(__dirname + '/html/index.html');  
});


app.get('/app.css', function (req, res) {
  res.sendfile(__dirname + '/app.css');  
});
*/

server.listen(app.get('port'),app.get('ipaddr'),function(){

	console.log('server listening on port'+app.get('port')+'and ip'+app.get('ipaddr'));
	console.log('ip address '+app.get('ipaddr'));
});


// usernames which are currently connected to the chat
var usernames = {};

io.sockets.on('connection', function (socket) {
	socket.emit("test",({"hello":"world"}));
	});

