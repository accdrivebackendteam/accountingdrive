var mongoose=require('mongoose');

var sch_company=new mongoose.Schema(
{
	name:{ type: String, unique: true, required: true },
	alias:String,
	address:String,
	country:String,
	state:String,
	city:String,
	pincode:String,
	telephoneno:String,
	email:String,
	currencysymbol:String,
	financialyearstartfrom:Date,
	administrator:mongoose.Schema.ObjectId,
	usersassociated:[mongoose.Schema.ObjectId],
	currencyname:String,
	decimalplaces:Number,
	showinmillions:Boolean,
	spacebtwamountandsymbol:Boolean,
	decimalplacesforprint:Number,
	symbolfordecimal:String,
	createdby:mongoose.Schema.ObjectId,
	modifiedby:mongoose.Schema.ObjectId,
	createdon:{ type: Date, default: Date.now },
	modifiedon:{ type: Date, default: Date.now },
	vouchertypeclass:[
					{name:String,
					includeledgergroups:[mongoose.Schema.ObjectId],//ledger group ids
					excludeledgergroups:[mongoose.Schema.ObjectId],//ledger group ids
					vouchertype:[{
								name:String,
								alias:String,
								isaccountingvoucher:Boolean,
								abbreviation:String,
								methodofvouchering:String,
								preventdupliate:Boolean,
								advanceconfiguration:Boolean,
								startingnumber:Number,
								widthofnumericalphabet:Number,
								prefilwithzero:Boolean,
								effectivedates:Boolean,
								optionaldefault:Boolean,
								commonnarration:Boolean,
								narrationforeachentry:Boolean,
								printaftersave:Boolean,
								printreceiptaftersave:Boolean,
								stockinventory:Boolean,
								vouchersunder:mongoose.Schema.ObjectId,
								vouchertyperestart:[{
									applicablefrom:Date,
									startingnumber:Number,
									particulars:String,
									vouchertypeprefix:String,
									vouchertypesuffix:String,
									createdby:mongoose.Schema.ObjectId,
									modifiedby:mongoose.Schema.ObjectId,
									createdon:{ type: Date, default: Date.now },
									modifiedon:{ type: Date, default: Date.now }
									}],
								createdby:mongoose.Schema.ObjectId,
								createdon:{ type: Date, default: Date.now },
								modifiedby:mongoose.Schema.ObjectId,
								modifiedon:{ type: Date, default: Date.now }
								}],
					createdby:mongoose.Schema.ObjectId,
					createdon:{ type: Date, default: Date.now },
					modifiedby:mongoose.Schema.ObjectId,
					modifiedon:{ type: Date, default: Date.now }}]
});

mongoose.model('mod_company',sch_company,'company');
