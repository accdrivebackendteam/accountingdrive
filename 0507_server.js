var app = require('express')()
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server);
  
var db=require('./model_mysql/mysqldb');
var ruser=require('./routes/user');
var ckuser = require('./routes/chkuser');
var license = require('./routes/crlicense');
var sufunc = require('./routes/sufunction');
var company = require('./routes/company')
app.configure(function(){
	app.set('port',process.env.OPENSHIFT_INTERNAL_PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080);
	app.set('ipaddr', process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1");
	});


// routing
app.get('/html/*', function (req, res) {
path = req.params[0] ? req.params[0] : 'index.html';
  res.sendfile(__dirname + '/html/'+path);  
});

server.listen(app.get('port'),app.get('ipaddr'),function(){

	console.log('server listening on port'+app.get('port')+'and ip'+app.get('ipaddr'));
});


// usernames which are currently connected to the chat
var usernames = {};
io.set('log level', 1);
io.sockets.on('connection', function (socket) {
	socket.emit("test",({"hello":"world"}));
	
	socket.on("su_createlicense",function(data){
		var connid = socket.id;
		sufunc.su_createlicense(connid,data.email,data.registerfor,data.price,data.number,data.expireson,data.companyname,callback);
	});

	socket.on("testconnection",function(data){
		var connid = socket.id;
		socket.emit("testresult",({"connectionid":connid}));
	});

	socket.on("getuserlicense",function(data){
		console.log('in su_getuserlicense');
		var connid = socket.id;
		license.getuserlicense(connid,callback);
	});
		
	socket.on("getcompanylicense",function(data){
		console.log('in su_getcompanylicense');
		var connid = socket.id;
		license.getcompanylicense(connid,callback);
	});
			
	 socket.on("su_deleteuserlicense",function(data){
		console.log('in su_deleteuserlicense');
		var connid = socket.id;
		sufunc.su_deleteuserlicense(connid,data.licenseid,callback);
	});
	
	 socket.on("su_deletecompanylicense",function(data){
		console.log('in su_deletecompanylicense');
		var connid = socket.id;
		sufunc.su_deletecompanylicense(connid,data.licenseid,callback);
	});
			
	socket.on("su_edituserlicense",function(data){
		console.log('in su_edituserlicense');
		var connid = socket.id;
		sufunc.su_edituserlicense(connid,data.docid,data.useremail,data.userid,data.price,data.boughton,data.expireson,callback);
	});
	
	socket.on("su_editcompanylicense",function(data){
		console.log('in su_editcompanylicense');
		var connid = socket.id;
		sufunc.su_editcompanylicense(connid,data.docid,data.companyid,data.price,data.boughton,data.expireson,callback);
	});
			
	socket.on("generatepassword",function(data){
		passwd.encryptpassword(data,callback);
	});
			
	socket.on("login",function(data){
		var IPaddress = socket.handshake.address.address;
		var connid = socket.id;
		console.log("in login fn");
		ruser.login(data.email,data.password,IPaddress,connid,callback);
	});
						
	socket.on("userhasunusedcompanylicense",function(data){
		var connid = socket.id;
		console.log('Inside User Has Unused Company License');
		company.userhasunusedcompanylicense(connid,callback);
	});
		
	socket.on("hasadduserlicense",function(data){
		var connid = socket.id;
		ruser.hasadduserlicense(connid,callback);
	});
		
	socket.on("addcompany",function(data){
		var connid = socket.id;
		console.log('From Server '+JSON.stringify(data));
		company.addcompany(data,connid,callback);
	});
				
	socket.on("logout",function(data){
		var connid = socket.id;
		ruser.logout(connid,data.user_id,callback);
	});
		
	socket.on("setuserrightsforcompany",function(data){
		var connid = socket.id;
		rights.setuserrightsforcompany(connid,data.companyid,data.userrights,callback);
	});
			
	socket.on("getuserrightsforcompany",function(data){
		rights.getuserrightsforcompany(data.userid,data.companyid,callback);
	});
		
	socket.on("addusertocompany",function(data){
		var connid = socket.id;
		company.addusertocompany(connid,data.companyid,data.emailid,callback);
	});
		
	socket.on("removeuserfromcompany",function(data){
		company.removeuserfromcompany(connid,data.email,data.companyid,callback);
	});
		
	socket.on("getuserlicensedetails",function(data){
		var connid = socket.id;
		company.getuserlicensedetails(connid,callback);
	});		
	
	socket.on("forcelogout",function(data){
		ruser.forcelogout(data.email,callback);
	});
	
	socket.on("settoken",function(data){
		var connid = socket.id;
		ckuser.settoken(data.tokenid,connid,callback);
	});
	
	socket.on("getgroups",function(data){
		var connid = socket.id;
		ckuser.settoken(data.companyid,connid,callback);
	});
	
	socket.on("getassociatedcompanylist",function(data){
		var connid=socket.id;
		ruser.getassociatedcompanylist(connid,callback);
	});
	
	socket.on("getcompanydetails",function(data){
<<<<<<< HEAD
		var connid=socket.id;
		company.getcompanydetails(connid,companyid,callback);
	});
=======
			var connid=socket.id;
			company.getcompanydetails(data.companyid,connid,callback);
		});
>>>>>>> 465804e447bcd80cfc4e2c18196289058266642c
	
    function callback(reply){
		console.log("Result from function"+JSON.stringify(reply));
		console.log("Error from function"+JSON.stringify(reply.response.msg));
        socket.emit(reply.action,reply.response);
    }
    
});

