var mongoose=require('mongoose');

var sch_group=new mongoose.Schema(
{
	name:{ type: String, required: true },
	isprimary:Boolean,
	isdefault:Boolean,
	nature:String,
	createdon:{ type: Date, default: new Date() },
	modifiedon:{ type: Date, default: new Date() },
	createdby:mongoose.Schema.ObjectId,
	modifiedby:mongoose.Schema.ObjectId
});

mongoose.model('mod_group',sch_group,'group');

var sch_groupunder=new mongoose.Schema(
{
	parentgroup:mongoose.Schema.ObjectId, 
	childgroup:mongoose.Schema.ObjectId
});

mongoose.model('mod_groupunder',sch_group,'groupunder');

var sch_companygroup=new mongoose.Schema(
{
		company:mongoose.Schema.ObjectId,
		groupid:mongoose.Schema.ObjectId,
		
});

mongoose.model('mod_companygroup',sch_group,'companygroup');
