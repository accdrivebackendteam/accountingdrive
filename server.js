var app = require('express')()
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server);
  
var db=require('./model_mysql/mysqldb');
var ruser=require('./routes/user');
var ckuser = require('./routes/chkuser');
var license = require('./routes/crlicense');
var sufunc = require('./routes/sufunction');
var company = require('./routes/company');
var rights = require('./routes/rights');
var ledger = require('./routes/ledger');
var groups = require('./routes/groups');
var units_msr = require('./routes/unitofmeasure');
var stkgrp = require('./routes/stockgroups');
var stkctg = require('./routes/stockcategories');

app.configure(function(){
	app.set('port',process.env.OPENSHIFT_INTERNAL_PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080);
	app.set('ipaddr', process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1");
	});


// routing
app.get('/html/*', function (req, res) {
path = req.params[0] ? req.params[0] : 'index.html';
  res.sendfile(__dirname + '/html/'+path);  
});

server.listen(app.get('port'),app.get('ipaddr'),function(){

	console.log('server listening on port'+app.get('port')+'and ip'+app.get('ipaddr'));
});


// usernames which are currently connected to the chat
var usernames = {};
io.set('log level', 1);
io.sockets.on('connection', function (socket) {
	socket.emit("test",({"hello":"world"}));
	
	socket.on("su_createlicense",function(data){
		var connid = socket.id;
		sufunc.su_createlicense(connid,data.email,data.price,data.user,data.expireson,data.company,callback);
	});

	socket.on("testconnection",function(data){
		var connid = socket.id;
		socket.emit("testresult",({"connectionid":connid}));
	});

	socket.on("getuserlicense",function(data){
		console.log('in su_getuserlicense');
		var connid = socket.id;
		license.getuserlicense(connid,callback);
	});
		
	socket.on("getcompanylicense",function(data){
		console.log('in su_getcompanylicense');
		var connid = socket.id;
		license.getcompanylicense(connid,callback);
	});
			
	 socket.on("su_deleteuserlicense",function(data){
		console.log('in su_deleteuserlicense');
		var connid = socket.id;
		sufunc.su_deleteuserlicense(connid,data.licenseid,callback);
	});
	
	 socket.on("su_deletecompanylicense",function(data){
		console.log('in su_deletecompanylicense');
		var connid = socket.id;
		sufunc.su_deletecompanylicense(connid,data.licenseid,callback);
	});
			
	socket.on("su_edituserlicense",function(data){
		console.log('in su_edituserlicense');
		var connid = socket.id;
		sufunc.su_edituserlicense(connid,data.docid,data.useremail,data.userid,data.price,data.boughton,data.expireson,callback);
	});
	
	socket.on("su_editcompanylicense",function(data){
		console.log('in su_editcompanylicense');
		var connid = socket.id;
		sufunc.su_editcompanylicense(connid,data.docid,data.companyid,data.price,data.boughton,data.expireson,callback);
	});
			
	socket.on("generatepassword",function(data){
		passwd.encryptpassword(data,callback);
	});
			
	socket.on("login",function(data){
		var IPaddress = socket.handshake.address.address;
		var connid = socket.id;
		console.log("in login fn");
		ruser.login(data.email,data.password,IPaddress,connid,callback);
	});
						
	socket.on("userhasunusedcompanylicense",function(data){
		var connid = socket.id;
		console.log('Inside User Has Unused Company License');
		company.userhasunusedcompanylicense(connid,callback);
	});
		
	socket.on("hasadduserlicense",function(data){
		var connid = socket.id;
		ruser.hasadduserlicense(connid,callback);
	});
		
	socket.on("addcompany",function(data){
		var connid = socket.id;
		console.log('From Server '+JSON.stringify(data));
		company.addcompany(data,connid,callback);
	});
				
	socket.on("logout",function(data){
		var connid = socket.id;
		ruser.logout(connid,data.user_id,callback);
	});
		
	socket.on("setuserrightsforcompany",function(data){
		var connid = socket.id;
		rights.setuserrightsforcompany(connid,data.companyid,data.update_userid,data.userrights,callback);
	});
			
	socket.on("getuserrightsforcompany",function(data){
		rights.getuserrightsforcompany(data.userid,data.companyid,callback);
	});
		
	socket.on("addusertocompany",function(data){
		var connid = socket.id;
		company.addusertocompany(connid,data.companyid,data.emailid,callback);
	});
		
	socket.on("removeuserfromcompany",function(data){
		company.removeuserfromcompany(connid,data.email,data.companyid,callback);
	});
		
	socket.on("getuserlicensedetails",function(data){
		var connid = socket.id;
		company.getuserlicensedetails(connid,callback);
	});		
	
	socket.on("forcelogout",function(data){
		ruser.forcelogout(data.email,callback);
	});
	
	socket.on("settoken",function(data){
		var connid = socket.id;
		ckuser.settoken(data.tokenid,connid,callback);
	});
	
	/////////GROUPS BLOCK///////////
	socket.on("getall_group",function(data){
		console.log('getallgroup');
		var connid = socket.id;
		groups.getall_group(connid,data.companyid,callback);
	});
	
	socket.on("getdetails_group",function(data){
		var connid = socket.id;
		groups.getdetails_group(connid,data.groupid,data.companyid,callback);
	});
	
	socket.on("create_group",function(data){
		var connid = socket.id;
		groups.create_group(connid,data.groupdata,data.companyid,callback);
	});
	
	socket.on("update_group",function(data){
		var connid = socket.id;
		groups.update_group(connid,data.companyid,data.groupid,data.groupdata,callback);
	});
	
	socket.on("delete_group",function(data){
		var connid = socket.id;
		groups.delete_group(connid,data.groupid,data.companyid,callback);
	});
	
	/////////////END OF GROUPS BLOCK////////////
	socket.on("getassociatedcompanylist",function(data){
		var connid=socket.id;
		ruser.getassociatedcompanylist(connid,callback);
	});
	
	socket.on("getcompanydetails",function(data){
		var connid=socket.id;
		company.getcompanydetails(data.companyid,connid,callback);
	});
	socket.on("updatecompanydetails",function(data){
		var connid=socket.id;
		company.updatecompanydetails(connid,data.companyid,data.companydetails,callback);
	});
	
	socket.on("getusermappeddetails",function(data){
		var connid=socket.id;
		company.updatecompanydetails(connid,data.companyid,callback);
	});
	
	socket.on("unuseduserlicense",function(data){
		var connid=socket.id;
		license.unuseduserlicense(connid,callback);
	});
	
	socket.on("createledger",function(data){
		var connid=socket.id;
		ledger.createledger(connid,data.ledgerdata,data.companyid,callback);
	});
	
	socket.on("createuser",function(data){
		var connid=socket.id;
		sufunc.createuser(connid,data.companyid,data.email,callback);
	});
	
	/////////UNITS OF MEASURE BLOCK////////////
	socket.on("getall_simple_unitsofmeasure",function(data){
		var connid=socket.id;
		units_msr.getall_simple_unitsofmeasure(connid,data.companyid,callback);
	});
	
	socket.on("getdetails_simple_unitsofmeasure",function(data){
		var connid=socket.id;
		units_msr.getdetails_simple_unitsofmeasure(connid,data.unitid,data.companyid,callback);
	});
	
	socket.on("create_simple_unitsofmeasure",function(data){
		var connid=socket.id;
		units_msr.create_simple_unitsofmeasure(connid,data.companyid,data.unitsofmeasure,callback);
	});
	
	socket.on("update_simple_unitsofmeasure",function(data){
		var connid=socket.id;
		console.log('UNITS OF MEASURE '+JSON.stringify(data.unitsofmeasure));
		units_msr.update_simple_unitsofmeasure(connid,data.unitid,data.unitsofmeasure,data.companyid,callback);
	});
	
	socket.on("delete_simple_unitsofmeasure",function(data){
		var connid=socket.id;
		units_msr.delete_simple_unitsofmeasure(connid,data.unitid,data.companyid,callback);
	});
	
	socket.on("getall_compound_unitsofmeasure",function(data){
		var connid=socket.id;
		units_msr.getall_compound_unitsofmeasure(connid,data.companyid,callback);
	});
	
	socket.on("getdetails_compound_unitsofmeasure",function(data){
		var connid=socket.id;
		units_msr.getdetails_compound_unitsofmeasure(connid,data.unitid,data.companyid,callback);
	});
	
	socket.on("create_compound_unitsofmeasure",function(data){
		var connid=socket.id;
		units_msr.create_compound_unitsofmeasure(connid,data.companyid,data.unitsofmeasure,callback);
	});
	
	socket.on("update_compound_unitsofmeasure",function(data){
		console.log('COMPOUDN UNITS OF MEASURE '+JSON.stringify(data.unitsofmeasure));
		var connid=socket.id;
		units_msr.update_compound_unitsofmeasure(connid,data.unitid,data.unitsofmeasure,data.companyid,callback);
	});
	
	socket.on("delete_compound_unitsofmeasure",function(data){
		var connid=socket.id;
		units_msr.delete_compound_unitsofmeasure(connid,data.unitid,data.companyid,callback);
	});
	///////////END OF UNITS OF MEASURE BLOCK/////////
	
	/////////STOCK GROUPS GROUPS BLOCK////////////
	socket.on("getall_stockgroups",function(data){
		var connid=socket.id;
		stkgrp.getall_stockgroups(connid,data.companyid,callback);
	});
	
	socket.on("getdetails_stockgroups",function(data){
		var connid=socket.id;
		stkgrp.getdetails_stockgroups(connid,data.stk_groupid,data.companyid,callback);
	});
	
	socket.on("create_stockgroups",function(data){
		var connid=socket.id;
		stkgrp.create_stockgroups(connid,data.stockgroupdata,data.companyid,callback);
	});
	
	socket.on("update_stockgroups",function(data){
		var connid=socket.id;
		stkgrp.update_stockgroups(connid,data.stk_groupid,data.stockgroupdata,data.companyid,callback);
	});
	
	socket.on("delete_stockgroups",function(data){
		var connid=socket.id;
		stkgrp.delete_stockgroups(connid,data.stk_groupid,data.companyid,callback);
	});
	///////////END OF STOCK GROUPS BLOCK/////////
	///////////STOCK CATEGORIES BLOCK////////////
	socket.on("getall_stockcategories",function(data){
		var connid=socket.id;
		stkctg.getall_stockcategories(connid,data.companyid,callback);
	});
	
	socket.on("getdetails_stockcategories",function(data){
		var connid=socket.id;
		console.log('STOCK CATG '+JSON.stringify(data));
		stkctg.getdetails_stockcategories(connid,data.stk_catgid,data.companyid,callback);
	});
	
	socket.on("create_stockcategories",function(data){
		var connid=socket.id;
		stkctg.create_stockcategories(connid,data.stockcatgdata,data.companyid,callback);
	});
	
	socket.on("update_stockcategories",function(data){
		var connid=socket.id;
		stkctg.update_stockcategories(connid,data.stk_catgid,data.stockcatgdata,data.companyid,callback);
	});
	
	socket.on("delete_stockcategories",function(data){
		var connid=socket.id;
		stkctg.delete_stockcategories(connid,data.stk_catgid,data.companyid,callback);
	});
	///////////END OF STOCK CATEGORIES BLOCK/////
	
	
    function callback(reply){
		console.log("Result from function"+JSON.stringify(reply));
        socket.emit(reply.action,reply.response);
    }
    
});

